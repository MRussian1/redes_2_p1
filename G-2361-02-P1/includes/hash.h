/**
 * @file hash.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Define una tabla hash
 *
 */

#ifndef HASH_H
#define HASH_H
#include "list.h"
#define HASH_SIZE 64

/**
 *@brief tipo de la tabla hash
 */
typedef _LIST* HASH;

/**
 * initHash
 * @brief Inicializa una tabla hash
 * @return la tabla hash inicializada
 */
HASH initHash();

/**
 * insertHash
 * @brief Inserta un elemento en la tabla hash, si no se4 encuentra ya
 * @param el elemento a introducir 
 * @param la tabla hash en la que guardarlo
 * @return OK si se introduce correctamente, ERROR en caso contrario
 */
STATUS insertHash(ELELIST*, HASH);

/**
 * isInHash
 * @brief Comprueba si un elemento  está en la tabla hash
 * @param el elemento a buscar 
 * @param la tabla hash en la que buscarlo
 * @return TRUE si esta en la tabla, FALSE si no
 */
BOOL isInHash(ELELIST*, HASH);

/**
 * get
 * @brief Obtiene el elemento almacenado que tiene el id dado
 * @param el id del elemento 
 * @param la tabla hash donde buscarlo
 * @return el elemento, o NULL si no se encuentró
 */
const ELELIST* get(ELELIST*, HASH);

/**
 * destroyHash
 * @brief Libera las estructuras asociadas a la tabla hash
 * @param la tabla hash a destruir
 */
void destroyHash(HASH*);
#endif
