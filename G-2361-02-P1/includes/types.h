/**
 * @file types.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Funciones de tipos
 *
 */
#ifndef TYPES_H
#define TYPES_H
/**
 * @brief enumerado que indica el estado de la funcion
 */
typedef enum{ERR = 0/**<Estado Error*/, OK/**<Estado OK*/} STATUS;
#ifdef	__cplusplus
extern "C" {
#else
/**
 * @brief enumerado que indica si verdadero o falso
 */
typedef enum{FALSE = 0/**<Valor falso*/, TRUE/**<Valor verdadero*/} BOOL;
#endif

#ifdef	__cplusplus
}
#endif

#endif
