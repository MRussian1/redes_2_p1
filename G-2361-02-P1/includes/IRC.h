/**
 * @file IRC.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Funciones del servidor IRC
 *
 */

#ifndef IRC_H
#define	IRC_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <redes2/irc.h>
#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <errno.h>
#include "../includes/COserver.h"

#define PASS_size 50
#define NICK_size 10
#define USER_size 50
#define HOST_size 100
#define PORT_size 2
#define IP_size 50
#define MAX_MSG_LEN 512
#define SERVER_NAME "Going_Merry"
#define SERVER_DESC "Barco de la tripulación de sombrero de paja Luffy"
#define VERSION_NAME "Monkey_D._Luffy"

/**
 * @brief Estructura que define campos necesarios en el IRC
 */
typedef struct ExtraInfo {
    int chatting; /**< Flag que define si el usuario esta correctamente loggeado o no*/
    int okPass; /**< Flag para saber si la password esta seteada*/
    int okNick; /**< Flag para saber si el nick esta seteado*/
    char password[PASS_size]; /**< Password*/
    char nick[NICK_size]; /**< Nick*/
    char user [USER_size];/**< USER*/
    char host[HOST_size]; /**< Hostname*/
    char ip[IP_size]; /**< Ip*/
    uint16_t port; /**< Port*/
    time_t idle; /**< idle time*/
    int desc; /**<descriptor de socket*/
} ExtraInfo;

/**
 * @brief Enumeracion de las posibles salidas de las funciones
 */
typedef enum {
    __PONG,
    __PING,
    __CLOSE,
    __OK,
    __NOMSG, 
    __ERR
} MSG_EXIT;


/**
 * @brief Funcion que parsea una lista de comandos IRC y envia una respuesta
 * @param question cadena a parsear
 * @param answer cadena de respuesta
 * @param desc descriptor de fichero
 * @param extrainfo informacion necesaria
 * @return MSG_EXIT codigo de accion
 */
MSG_EXIT parseMessage(char * question, int desc, ExtraInfo * extrainfo);

/**
 * @brief Funcion que parsea una comando IRC y envia una respuesta
 * @param command cadena a parsear
 * @param answer cadena de respuesta
 * @param desc descriptor de fichero
 * @param extrainfo informacion necesaria
 * @return Accion que se prevee que realice el código exterior
 *         __OK Enviar mensage y liberar answer
 *         __NOMSG No enviar mensaje
 *         __CLOSE cerrar hilo
 *         __PING Enviar mensage y liberar answer
 *         __PONG No enviar mensaje y detectar pong
 */
MSG_EXIT parseCommandGeneral (char * command, int desc, ExtraInfo * extrainfo);

/**
 * @brief Funcion que parsea un comando IRC
 * @param command cadena a parsear
 * @param respuesta (no reservada)
 * @param informacion extra
 * @return Accion que se prevee que realice el código exterior
 *         __OK Enviar mensage y liberar answer
 *         __NOMSG No enviar mensaje
 *         __CLOSE cerrar hilo
 *         __PING Enviar mensage y liberar answer
 *         __PONG No enviar mensaje y detectar pong
 */
MSG_EXIT parseCommand (char * command, char ** answer, ExtraInfo * extrainfo);

/**
 * @brief Funcion que parsea los comandos de no conexión IRC
 * @param command comando coman
 * @param respuesta (no reservada)
 * @param informacion extra
 * @return Accion que se prevee que realice el código exterior
 *         __OK Enviar mensage y liberar answer
 *         __NOMSG No enviar mensaje
 *         __CLOSE cerrar hilo
 *         __PING Enviar mensage y liberar answer
 *         __PONG No enviar mensaje y detectar pong
 */
MSG_EXIT parseNotConnected (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que parsea un comando los comandos que se ejecutan
 		en conexión IRC
 * @param command comando coman
 * @param respuesta (no reservada)
 * @param informacion extra
 * @return Accion que se prevee que realice el código exterior
 *         __OK Enviar mensage y liberar answer
 *         __NOMSG No enviar mensaje
 *         __CLOSE cerrar hilo
 *         __PING Enviar mensage y liberar answer
 *         __PONG No enviar mensaje y detectar pong
 */
MSG_EXIT parseConnected(char * command, char ** answer,  ExtraInfo * extrainfo);

/**
 * @brief genera la respuesta de whois
 * @param answer respuesta generada
 * @param extrainfo informacion extra
 * @param target channel or user that you want to know who is
 * @return 0
 */
int whois_rpl(char ** answer, ExtraInfo* extrainfo, char * target);

/**
 * @brief genera la respuesta de user
 * @param answer respuesta generada
 * @param extrainfo informacion extra
 * @return 0
 */
int user_rpl(char ** answer, ExtraInfo* extrainfo);

/**
 * @brief genera la respuesta de motd
 * @param answer respuesta generada
 * @param extrainfo informacion extra
 * @return 0
 */
int motd_apply(char ** answer, ExtraInfo* extrainfo);

/**
 * @brief genera la respuesta de join
 * @param answer respuesta generada
 * @param extrainfo informacion extra
 * @param channel canal a unirse
 * @param key clave de dicho canal
 * @return 0
 */
int join_rpl(char ** answer, ExtraInfo * extrainfo, char * channel, char * key);

/**
 * @brief genera la respuesta de list
 * @param answer respuesta generado
 * @param extrainfo informacion extra
 * @param mask mascara de list
 * @return 0
 */
int list_rpltodo(char ** answer, ExtraInfo* extrainfo, char * mask);

/**
 * @brief Crea la la respuesta de names
 * @param answer respuesta
 * @param extrainfo informacion necesaria
 * @param channel canal del que se desean los usuarios
 * @return la respuesta en answer
 */
int names_rpltodo(char ** answer, ExtraInfo* extrainfo, char * channel);

/**
 * @brief thensforma una lista de usuarios en una cadena válida que marca a
		los usuarios OP con @
 * @param ans respuesta enviada
 * @param list lista de elementos
 * @param num numero de elementos de la lista
 * @param channel canal del que los usuarios son op o no
 */
int oplist (char ** ans, char ** list,long num, char * channel);

/**
 * @brief genera una cadena de una lista de canales para un usuario
 * @param ans la lista de canales
 * @param lista de canales el los que esta el usuario
 * @param numero de usuarios de la lista 
 * @param nick nick del usuario
 */
int chanlist (char ** ans, char ** list,long num, char * nick);

/**
 * @brief Funcion que genera la respuesta de nick cuando no esta conectado
 		y guarda dicho nick
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseNotConnectedNick (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de user cuando no esta conectado, y cambia el estado a hablando
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseNotConnectedUser (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de nick cuando esta conectado y cambia el nick
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedNick (char * command, char ** answer,
		ExtraInfo * extrainfo );

/**
 * @brief Funcion que genera la respuesta de topic
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedTopic (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de part y hace que el usuario se vaya del canal
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedPart (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de kick y expulsa al usuario del canal
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedKick (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de privmsg
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return Accion que se prevee que realice el código exterior
 *         __OK Enviar mensage y liberar answer
 *         __NOMSG No enviar mensaje
 */
MSG_EXIT parseConnectedPrivMsg (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que manda el mensaje a todos los usuarios del canal
 * @param msgtosend mensaje que sera enviado a todos los usuarios
 * @param extrainfo informacion extra necesaria
 * @param channel canal al que se envia el mensaje
 * @return -1 si error, 0 si OK
 */
int msgToChan (char * msgtosend,  ExtraInfo * extrainfo, char *channel);

/**
 * @brief Funcion que genera la respuesta de ping
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __PING si bien __OK si error
 */
MSG_EXIT parseConnectedPing (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de pong
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __PONG sibien __OK si error
 */
MSG_EXIT parseConnectedPong (char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de join
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedJoin(char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de mode
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedMode(char * command, char ** answer,
		ExtraInfo * extrainfo);

/**
 * @brief Funcion que genera la respuesta de away
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedAway(char * command, char ** answer,
		ExtraInfo * extrainfo);

 /**
 * @brief Funcion que genera la respuesta de mode para un canal y usuario
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @param channel canal del que se intenta cambiar el modo
 * @param user usuario al que se le intenta cambiar el modo
 * @param mode modo cambiado
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT mode_userOnChannel(char **answer, ExtraInfo * extrainfo,
		char * channel, char * user, char * mode);

 /**
 * @brief Funcion que genera la respuesta de mode para un usuario
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @param user usuario al que se le intenta cambiar el modo
 * @param mode modo cambiado
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT mode_user (char **answer, ExtraInfo * extrainfo, char * user,
		char * mode);

 /**
 * @brief Funcion que genera la respuesta de mode para un canal 
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @param channel canal del que se intenta cambiar el modo
 * @param mode modo cambiado
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT mode_channel (char **answer, ExtraInfo * extrainfo, char * channel,
		char * mode);

 /**
 * @brief Funcion que genera la respuesta de mode para conoder el modo de un canal 
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @param channel canal del que se obtener el modo
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT mode_getMode(char **answer, ExtraInfo * extrainfo, char * channel);

/**
 * @brief Funcion que genera la respuesta de quit
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedQuit(char * command, char ** answer, ExtraInfo * extrainfo);
/**
 * @brief Funcion que genera la respuesta de Motd
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
 
MSG_EXIT parseConnectedMotd(char * command, char ** answer, ExtraInfo * extrainfo);
/**
 * @brief Funcion que genera la respuesta de whois
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
 
MSG_EXIT parseConnectedWhois(char * command, char ** answer, ExtraInfo * extrainfo);
/**
 * @brief Funcion que genera la respuesta de List
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedList(char * command, char ** answer, ExtraInfo * extrainfo);
/**
 * @brief Funcion que genera la respuesta de Names
 * @param command comando de entrada
 * @param answer comando de respuesta
 * @param extrainfo informacion extra
 * @return MSG_EXIT __OK siempre
 */
MSG_EXIT parseConnectedNames(char * command, char ** answer,ExtraInfo * extrainfo);
/**
 *@brief Crea la información asociada a extrainfo
 *@param desc descriptor de fichero abierto
 *@return puntero a ExtraInfo
 */
ExtraInfo* extraInfoCreate(int desc);

/**
 * @brief libera extrainfo
 * @param extrainfo puntero a liberar
 */
void freeInfo(ExtraInfo * extrainfo);

/**
 * @brief libera al usuario
 * @param username usuario a liberar
 *
 */
void freeUser(char* username);

#ifdef	__cplusplus
}
#endif

#endif	/* IRC_H */
