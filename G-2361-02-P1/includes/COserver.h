/**
 * @file COserver.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Funciones del servidor orientado a conexion
 *
 */

#ifndef SERVER_CONNECTION_H
#define	SERVER_CONNECTION_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <errno.h>
#include "msg.h"

#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#define MAXBUFF 1024
/**
 * @brief demoniza el servidor
 */    
void go_daemon ();  
/**
 * @brief Inicia el servidor orientado a conexión para aceptar conexiones
 * @param domain tipo de red a la que se conecta (AF_INET, AF_INET6, AF_UNIX)
 * @param port puerto de la conexion
 * @param numero máximo de conexiones que se aceptarán
 * @return descriptor de conexión
 */
int initiate_serverCO(int domain, uint16_t port, unsigned int max_connections);
/**
 * @brief Acepta una conexion TCP
 * @param sockval descriptor de conexión devuelto por initiate-serverCO
 * @return descriptor de nueva conexion 
 */
int accept_connectionCO(int sockval);



#ifdef	__cplusplus
}
#endif

#endif	/* SERVER_CONNECTION_H */

