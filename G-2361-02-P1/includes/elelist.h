/**
 * @file elelist.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Define un elemento de la lista
 *
 */



#ifndef ELELIST_H
#define ELELIST_H
#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

/**
 * @brief estructura de un elemento de la lista
 */
typedef struct {
 void* element; /**<Elemeto de la lista*/
 size_t length; /**<Longitud*/
} ELELIST;

/**
 *iniEleList
 * @brief : Crea un nuevo elemento del tamaño en bytes dado
 * y con la información contenida en la dirección dada
 * @param información a guardar en el nuevo elemento 
 * @param el segundo su tamaño.
 * @return  NULL en caso de algún fallo el puntero al nuevo elemento si no
 */
ELELIST* iniEleList(void*, size_t);

/**
 * getElelistInfo
 * @brief devuelve la información guardada en el ELELIST
 * @param el ELELIST a consultar
 * @return NULL en caso de rcibir o contener algún puntero nulo o
 * la información guardada en el ELELIST si no
*/
void* getElelistInfo(const ELELIST* e);

/**
 *getElelistInfo
 * @brief devuelve el tamaño de la información contenida en
 * el ELELIST
 * @param el ELELIST a consultar
 * @return la longitud de la información contenida
*/
size_t getElelistLength(const ELELIST* e);

/**
 *setCopyFcn
 * @brief Proporciona la función que copia los elementos
 * @param Funcion de copia conel primer argumento como destino y el segundo como fuente.
 * Para el destino, solo viene proporcionada la memoria que ocupa
 *  * el elemento de origen (segun su longitud)
 * Cualquier valor de retorno distinto de 0 se considera un error.
 * @return ERROR si el puntero es NULL u OK si no
*/
STATUS setCopyFcn (int(*fcn)(void*,void*));

/**
 *copyEleList
 * @brief Copia el elemento
 * @param elemento a copiar 
 * @param elemento que va a ser copiado
 * @return ERROR si ha habido error u OK si ha ido bien
 */
STATUS copyEleList (ELELIST*, const ELELIST*);

/**
 *setEqualFcn
 * @brief Proporciona la función que verifica la equivalencia
 * de dos elementos
 * @param función que recibe dos void* y devuelve un int. Cualquier valor
 * de retorno distinto de 0 se considera un desigualdad
 * @return ERROR si el puntero es NULL u OK si no
 */
STATUS setEqualFcn(int(*fcn)(void*,void*));

/**
 *equal
 * @brief averigua si dos elementos son equivalentes, es decir,
 * si sus id coinciden
 * @param los elementos a comparar
 * @return TRUE si son iguales, FALSE si no
 */
BOOL equal(const ELELIST*, const ELELIST*);

/**
 *setHashFcn
 * @brief Proporciona la función que calcula el hash del elemento
 * @param función que recibe un void* y devuelve un int. Si esta función
 * devuelve -1 se considerará un error
 * @return ERROR si el puntero es NULL u OK si no
*/
STATUS setHashFcn(uint32_t(*fcn)(void*));

/**
 *hashCode
 * @brief devuelve el codigo hash del elemento dado
 * @param el elemento cuyo codigo hash se desea calcular, y sobre qué
 * tamaño de tabla
 * @return el codigo hash del elemento
*/
int hashCode(const ELELIST*, int);

/**
 *setDestroyFcn
 * @brief Proporciona la función que libera la memoria de un elemento
 * @param función que recibe un void* y devuelve un void.
 * @return ERROR si el puntero es NULL u OK si no
*/
STATUS setDestroyFcn(void(*fcn)(void*));

/**
 *destroyEleList
 * @brief libera estructuras asociadas al elemento
 * @param el elemento a destruir
*/
void destroyEleList(ELELIST*);

#endif /* ELELIST_H */

