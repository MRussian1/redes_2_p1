/**
 * @file thread_module.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Funciones de hilos
 *
 */

#ifndef THREAD_MODULE_H
#define THREAD_MODULE_H
#define _GNU_SOURCE
#define _XOPEN_SOURCE 600
#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include "../includes/list.h"
#include "../includes/IRC.h"

#define MSG_BUF_SIZE 256
#define THREAD_JOIN_TIMEOUT 5

/**
 * @brief Prepara el módulo de hilos
 * Inicia el módulo de hilos y módulos de los que depende,
 * necesario para su funcionamento. También inicia las interrupciones
 * por alarmas, cuya freciencia viene dada por el ping
 * @param ping frecuencia de las alarmas, se usan internamente para ping y pong
 * @return ERR en caso de algún error (sin garantías de servicio), OK sino 
 */
STATUS setup_master(int ping);

/**
 * @brief Inicia un hilo de atención a un cliente
 * Crea un nuevo hilo que recibe mensajes de un cliente por el
 * descriptor de fichero dado, para luego procesar sus mensajes
 * y enviar respuestas según el módulo IRC.
 * @param fd socket desde el cual obtener y enviar mensajes al cliente
 * @return ERR en caso de algún error, OK de lo contrario 
 */
STATUS spawn_thread (int fd);

/**
 * @brief Informa al hilo el nombre de usuario del cliente
 * Para una mejor interacción con el módulo IRC, el hilo guarda la asociación
 * entre nombres de usuario y descriptores de fichero, que luego se puede
 * consultar (pero no cambiar) mediante la función getFDByUsername
 * @param fd socket del usuario
 * @param usrn cadena que represente el nombre de usuario
 * @return ERR en caso no haber podido establecer la asociación, OK de lo contrario 
 */
STATUS tellUsername(int fd, char* usrn);

/**
 * @brief Tiempo del último mensaje del usuario
 * @param fd descriptor de fichero que identifica al usuario
 * @return segundos desde la marca de tiempo UNIX (vease time()) transcurridos hasta el último mensaje del usuario dado, -1 en caso de error*/
int last_active(int fd);

/**
 * @brief Descriptor de fichero del usuario
 * @param usrn cadena que representa el nombre de usuario
 * @return socket del usuario dado, -1 en caso de error*/
int getFDByUsername(char* usrn);

/**
 * @brief Terminar este módulo y el programa
 * @param nsignal Debe estar a 0, de lo contrario el resultado es indefinido
 */
void clean_end (int nsignal);

#endif /*THREAD_MODULE_H*/
