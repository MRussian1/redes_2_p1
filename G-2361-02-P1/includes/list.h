/**
 * @file list.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Define una lista de elementos
 */

#ifndef LIST_H
#define	LIST_H
#include <stdio.h>
#include <stdlib.h>
#include "elelist.h"

/**
 * @brief estructura que define un nodo
 *
 */
typedef struct t_node{
    ELELIST data; /**< Datos del nodo*/
    struct t_node *next; /**< Siguiente nodo*/
} NODE;
 
/**
 * @brief define una lista de nodos
 */
typedef NODE* _LIST; 


/**
 *  getNode
 * @brief Inicializa un nodo
 * @return el nodo inicializado
 */
NODE* getNode();

/**
 *  freeNode
 * @brief Libera un nodo de la lista
 * @param puntero al nodo a liberar
 */
void freeNode(NODE*);

/**
 *  iniList
 * @brief Inicializa una lista
 * @param la lista a inicializar
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS iniList(_LIST*);

/**
 *  insertFirstEleList
 * @brief inserta un elemento al principio de la lista
 * @param un elemento y la lista donde insertarlo
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS insertFirstEleList (_LIST*, const ELELIST*);

/**
 *  insertLastEleList
 * @brief inserta un elemento al final de la lista
 * @param un elemento 
 * @param la lista donde insertarlo
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS insertLastEleList  (_LIST*, const ELELIST*);

/**
 *  extractFirstEleList
 * @brief Extrae primer elemento de la lista
 * @param un elemento  
 * @param la lista de donde extraerlo
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS extractFirstEleList (_LIST*, ELELIST*);

/**
 *  extractNthEleList
 * @brief Extrae el elemnto enesimo de la lista, o el mas cercano
 * que haya
 * @param un elemento 
 * @param la lista de donde extraerlo 
 * @param el numero empezando por 0, del elemento a extraer  
 * @param un indicador de si se lee sin extraer o no
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS extractNthEleList (_LIST*, ELELIST*, int, BOOL);

/**
 *  extractLastEleList
 * @brief Extrae el último elemento de la lista
 * @param un elemento y la lista de donde extraerlo
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS extractLastEleList (_LIST*, ELELIST*);

/**
 *  freeList
 * @brief Libera la lista
 * @param la lista
 * @return ERR si ha habido error o OK si ha ido bien
 */
STATUS freeList(_LIST*);

/**
 *  isEmptyList
 * @brief Comprueba si la lista está vacía
 * @param la lista
 * @return TRUE si la lista está vacía y FALSE en caso contrario
 */
BOOL isEmptyList(const _LIST*);

/**
 *  numElemsList
 * @brief Devuelve el número de elementos de la lista
 * @param la lista
 * @return el número de elementos de la lista
 */
int numElemsList(const _LIST*);

/**
 *  insertInOrderList
 * @brief inserta elemento en orden ascendente
 * @param un elemento  
 * @param la lista donde insertarlo
 */
STATUS insertInOrderList (_LIST*, const ELELIST*); 

/**
 *  isInList
 * @brief busca un elemento dado y reporta si se ha encontrado
 * @param un elemento  
 * @param la lista donde buscalo
 */
BOOL isInList(_LIST*, const ELELIST*);

/**
 *  searchInList
 * @brief busca un elemento dado y lo devuelve
 * @param un elemento  
 * @param la lista donde buscalo
 */
const ELELIST* searchInList(_LIST*, const ELELIST*);


#endif	/* LIST_H  */
