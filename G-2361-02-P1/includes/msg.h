/**
 * @file msg.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Funciones del mensajes de sockets
 *
 */

#ifndef MSG_H
#define	MSG_H

#ifdef	__cplusplus
extern "C" {
#endif

    
#include <stdint.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <sys/stat.h>
#include <errno.h>

/**
 * @brief Recibe un mensaje 
 * @param connval descriptor de socket
 * @param msg mensaje recibido
 * @param len longitud
 * @return longitud del mensaje, 0 si ser ha cerrado la conexión
 * desde el otro extremo o -1 si ha habido otro tipo de error
 * (vease errno en estos casos)
 */
int recv_msg(int connval, char * msg, int len);

/**
 * @brief Envia un mensaje
 * @param connval descriptor de socket
 * @param msg mensaje recibido
 * @param len longitud
 * @return longitud del mensaje o -1 en caso de error
 * (vease errno en estos casos)
 */
int send_msg(int connval, char * msg, int len);
    


#ifdef	__cplusplus
}
#endif

#endif	/* MSG_H */

