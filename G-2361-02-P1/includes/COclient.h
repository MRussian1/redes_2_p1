/**
 * @file COclient.h
 * @author Miguel Russian Rojas 
 * @author Ana Peraita Alonso
 * @brief Define el cliente orientado a conexion
 */

#ifndef TCPCLIENT_H
#define	TCPCLIENT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <stdlib.h>
#include <strings.h>
#include <netinet/in.h>
#include "msg.h"
/**
 * @brief Inicia el cliente orientado a conexión para aceptar conexiones
 * @param domain tipo de red a la que se conecta (AF_INET, AF_INET6, AF_UNIX)
 * @param port puerto de la conexion
 * @param numero máximo de conexiones que se aceptarán
 * @return descriptor de conexión
 */
int initiate_clientCO(int domain, int port, long ip);



#ifdef	__cplusplus
}
#endif

#endif	/* TCPCLIENT_H */

