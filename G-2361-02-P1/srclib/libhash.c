#include "../includes/hash.h"

HASH initHash() {
	return (HASH)calloc(HASH_SIZE, sizeof(_LIST*));
}

STATUS insertHash(ELELIST* e, HASH h) {
	if (e == NULL || h == NULL) {
		return ERR;
	}
	int code = hashCode(e, HASH_SIZE);
	if (code == -1) {
		return ERR;
	}
	if (h[code] == NULL) {
		iniList(h + code);
	}
	if (isInList(h + code, e) == FALSE) {
		return insertLastEleList(h + code, e);
	}
	return ERR;
}

BOOL isInHash(ELELIST* e, HASH h) {
	if (e == NULL || h == NULL) {
		return ERR;
	}
	int code = hashCode(e, HASH_SIZE);
	if (code == -1) {
		return FALSE;
	}
	return isInList(h + code, e); 
}

const ELELIST* get(ELELIST* e, HASH h) {
	if (e == NULL || h == NULL) {
		return NULL;
	}
	int code = hashCode(e, HASH_SIZE);
	if (code == -1) {
		return NULL;
	}
	return searchInList(h + code, e);
}

void destroyHash(HASH* h) {
	if (h == NULL || *h == NULL) {
		return;
	}
	for (int i = 0; i < HASH_SIZE; i++) {
		freeList(*h + i);
	}
	free(*h);
	*h = NULL;
}
