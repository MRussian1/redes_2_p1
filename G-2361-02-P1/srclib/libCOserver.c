#include "../includes/COserver.h"

/**
 * Funcion que demoniza un proceso
 *
 *
 */ 
/* NOTA : sudo netstat -peant | grep ":7 " para descubrir proceso escuchando en puerto*/
void go_daemon () {
	pid_t pid, miid;
	int i, tablesize;
	tablesize=getdtablesize();
	for(i=0; i<tablesize;i++){
		close(i);
	}
	pid = fork(); /* Fork off the parent process */
	if (pid < 0) exit(EXIT_FAILURE);
	if (pid > 0) exit(EXIT_SUCCESS); /* Exiting the parent process. */
	umask(0); /* Change the file mode mask */
	miid=getpid();
	setlogmask (LOG_UPTO (LOG_INFO)); /* Open logs here */
	openlog ("Server system messages:", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL3);
	syslog (LOG_ERR, "Initiating new server.");
	if (setsid()< 0) { /* Create a new SID for the child process */
		syslog (LOG_ERR, "Error creating a new SID for the child process.");
		exit(EXIT_FAILURE);
	}
	if ((chdir("/")) < 0) { /* Change the current working directory */
		syslog (LOG_ERR, "Error changing the current working directory = \"/\"");
		exit(EXIT_FAILURE);
	}
	syslog (LOG_INFO, "Mi ip es %d", miid);
	
	return;
}
	
/**
 * @brief Funcion que inicia el servidor 
 * @param domain Tipo de  red (A_INET)
 * @param port Puerto de conexion 
 * @param max_connections numero maximo de conexiones
 * @return descriptor de fichero para el accept, -1 en caso de error
 */

int initiate_serverCO(int domain, uint16_t port, unsigned int max_connections) {
        int sockval;
        struct sockaddr_in Direccion;
        
        syslog(LOG_INFO, "Creating socket");
        if( (sockval=socket(domain, SOCK_STREAM, 0)) < 0) {
             syslog(LOG_ERR, "Error creating socket");
             return -1;   
        }
        
        Direccion.sin_family = domain;
        Direccion.sin_port = htons(port);
        Direccion.sin_addr.s_addr = htonl(INADDR_ANY);
        bzero((void *)&(Direccion.sin_zero), 8);
        
        syslog (LOG_INFO, "Binding socket");
        if (bind(sockval, (struct sockaddr*)&Direccion, sizeof(Direccion)) < 0) {
            syslog(LOG_ERR, "Error binding socket");
            return -1; 
        }
        
        syslog (LOG_INFO, "Listening conections");
        if (listen(sockval, max_connections) < 0) {
            syslog(LOG_ERR, "Error listening");
            return -1; 
        }
        
        return sockval;
}

/**
 * @brief Acepta una conexion
 * @param sockval int devuelto por initiate server
 * @return descriptor de fichero para una conexion nueva
 */
int accept_connectionCO(int sockval) {
    int desc;
	socklen_t len;
        struct sockaddr Conexion;
        
        len = sizeof(Conexion);
        
        if ((desc = accept(sockval, &Conexion, &len)) < 0) {
             syslog(LOG_ERR, "Error accepting conection: %s", strerror(errno));
             return -1;
        }
   
    return desc;
}
/**
 * @brief recive un mensaje, lo procesa y envia la respuesta
 * @param connval valor devuelto por accept connection
 * @param process funcion que procesa el mensaje y envia 
 * @param msgin estructura del mensaje que se recibe (contiene msg->len (longitud) y msg->buf (void *) (debe de ser reservado fuera))
 * @param msgout estructura del mensaje que se envia (contiene msg->len (longitud) y msg->buf (void *) (debe de ser reservado fuera))
 */
/*
int process_connection_serverCO(int connval, int (* process)(conn_msg* msgin, conn_msg* msgout ), conn_msg * msgin, conn_msg * msgout){

		int err;
		syslog(LOG_INFO, "New access");
		//recibe mensaje de cliente
		
		msgin->len=recv(connval, msgin->buf, MAXBUFF, 0);
		
		if((err=process(msgin, msgout))==1){	
			send(connval, msgout->buf, msgout->len, 0);
   		}
		syslog(LOG_INFO, "Read something: %s %s", (char*)msgin->buf, (char *) msgout->buf);
		
		if(msgin->len ==0 || err==-1){
			return -1;
		}
		return msgin->len;
		
}

*/


