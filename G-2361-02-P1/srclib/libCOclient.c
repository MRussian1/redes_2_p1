
#include "../includes/COclient.h"

int initiate_clientCO(int domain, int port, long ip){
  
        int sockval;
        
        struct sockaddr_in Direccion;
        
        fprintf(stdin,"Creating socket");
        if( (sockval=socket(domain, SOCK_STREAM, 0)) < 0) {
             fprintf(stderr, "Error creating socket");
             exit(EXIT_FAILURE);   
        }
        
        Direccion.sin_family = domain;
        Direccion.sin_port = htons(port);
        Direccion.sin_addr.s_addr = htonl(ip);
        bzero((void *)&(Direccion.sin_zero), 8);
        
        puts ("Connecting to socket");
        if (connect(sockval, (struct sockaddr*)&Direccion, sizeof(Direccion)) < 0) {
                fprintf(stderr, "Error creating socket");
                exit(EXIT_FAILURE); 
        }
    return sockval;
}



