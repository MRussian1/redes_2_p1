#include "../includes/elelist.h"

int (*cpy) (void*, void*) = NULL;
int (*eq) (void*, void*) = NULL;
void (*destroy) (void*) = NULL;
uint32_t (*hsh) (void*) = NULL;

ELELIST* iniEleList(void* e, size_t len) {
	ELELIST* el = (ELELIST*)calloc(1, sizeof(ELELIST));
	if (el == NULL) {
		return NULL;
	}
	el->element = calloc(1, len);
	if (el->element == NULL) {
		free(el);
		return NULL;
	}
	memcpy(el->element, e, len);
	el->length = len;
	return el;
}

void* getElelistInfo(const ELELIST* e) {
	if (e == NULL)
		return NULL;
	return e->element;
}

size_t getElelistLength(const ELELIST* e) {
	if (e == NULL)
		return -1;
	return e->length;
}

STATUS setCopyFcn (int(*fcn)(void*,void*)) {
	if (fcn == NULL) {
		return ERR;
	}
	cpy = fcn;
	return OK;
}

STATUS copyEleList (ELELIST* dst, const ELELIST* src) {
	if (dst == NULL || src == NULL) {
		return ERR;
	
	}
	if(dst->element != NULL) {
		free(dst->element);
	}
	dst->element = calloc(1, src->length);
	if (dst->element == NULL) {
		return ERR;
	}
	dst->length = src->length;
    return (cpy(dst->element, src->element)) == 0 ? OK:ERR;
}

STATUS setEqualFcn(int(*fcn)(void*,void*)) {
	if (fcn == NULL) {
		return ERR;
	}
	eq = fcn;
	return OK;
}

BOOL equal(const ELELIST* e1, const ELELIST* e2) {
	if (e1 == NULL || e2 == NULL) {
		return (e1 == e2) ? TRUE:FALSE;
	}
	return (eq(e1->element, e2->element) == 0) ? TRUE:FALSE;
}

STATUS setHashFcn(uint32_t(*fcn)(void*)) {
	if (fcn == NULL) {
		return ERR;
	}
	hsh = fcn;
	return OK;
}

int hashCode(const ELELIST* e, int n) {
	uint32_t r;
	if (e == NULL) {
		return -1;
	}
	r = hsh(e->element);
	return ((r == -1) ? (r % n):-1);
}

STATUS setDestroyFcn(void(*fcn)(void*)) {
	if (fcn == NULL) {
		return ERR;
	}
	destroy = fcn;
	return OK;
}

void destroyEleList(ELELIST* e) {
	if (e == NULL) {
		return;
	}
	if (destroy != NULL) {
		destroy(e->element);
	}
	free(e->element);
	e->element = NULL;
}
