#include "../includes/msg.h"

/**
 *
 *
 */
int recv_msg(int connval, char * msg, int len){

 return recv(connval, msg, len, 0);

}

int send_msg(int connval, char * msg, int len) {
	int r = send(connval, msg, len, MSG_NOSIGNAL);
	if (r < 0 && errno == EINTR) {
		return send(connval, msg, len, MSG_NOSIGNAL);
	}
	return r;
}
