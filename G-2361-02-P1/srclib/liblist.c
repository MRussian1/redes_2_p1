#include "../includes/list.h"

NODE* getNode() {
    NODE* n = (NODE*)calloc(1, sizeof(NODE));
    if(!n) return NULL;
    n->next= NULL;
    return n;
}

void freeNode(NODE *node) {
    if(!node)
        return;
    destroyEleList(&(node->data));
    free(node);
}

STATUS iniList(_LIST *list) {
    if(!list) return ERR;
    *list = NULL;
    return OK;
}

STATUS insertFirstEleList (_LIST* list, const ELELIST* elem) {
    NODE* n;
    if(!list || !elem)
        return ERR;
    
    n = getNode();
    if(n) {
        if(isEmptyList(list) == TRUE)
            n->next = NULL; /*as last element, next shouldn't point to anything*/
        else 
            n->next = *list;
        *list = n;
        copyEleList(&(n->data), elem);
    }
    else
        return ERR; /*new node couldn't be allocated*/
    
    return OK;
}

STATUS insertLastEleList  (_LIST *list, const ELELIST *elem) {
    NODE *n, *p;
    if(!list || !elem)
        return ERR;
    
    n = *list; 
    
    p = getNode();
    if(p) {
        if(n) {
            for(; n->next != NULL; n = n->next); /*get last element*/
            n->next= p;
        }
        else
            *list = p;
        copyEleList(&(p->data), elem);
        p->next = NULL;
    }
    else
        return ERR;
    
    return OK;
}

STATUS extractFirstEleList (_LIST *list, ELELIST* elem) {
    NODE* n;
    if(!list || !elem || isEmptyList(list))
        return ERR;
    
    n = (*list)->next;
    copyEleList(elem, &((*list)->data));
    freeNode(*list);
    *list = n;
    return OK;
}

STATUS extractNthEleList (_LIST *list, ELELIST* elem, int nth, BOOL read) {
    NODE *n, *p;
    int i;
    if(!list || !elem || nth < 0 || isEmptyList(list))
        return ERR;
    
    n= *list;
    if (numElemsList(list) < 2) {
        copyEleList(elem, &(n->data));
        if (!read) {
			freeNode(n);
			*list = NULL;
        }
    } else {
		for(i = 0, n = *list; (n->next->next != NULL) && (i < (nth - 1));
				n = n->next, i++);
		
		copyEleList(elem, &(n->next->data));
		if (!read) {		
			p = n->next->next;
			freeNode(n->next);
			n->next= p;
		}
	}
    return OK;
}

STATUS extractLastEleList (_LIST *list, ELELIST* elem) {
    NODE* n;
    if(!list || !elem || isEmptyList(list))
        return ERR;
    
    n= *list;
    if (numElemsList(list) < 2) {
        copyEleList(elem, &(n->data));
        freeNode(n);
        *list = NULL;
    } else {
        for(n = *list; n->next->next != NULL; n = n->next); /*get second-to-last element*/
        
        copyEleList(elem, &(n->next->data));
        freeNode(n->next);
        n->next= NULL;
    }
    return OK;
}

STATUS freeList(_LIST *list) {
    ELELIST e = {NULL, 0}; //Broken modularity!
    
    if(!list)
        return ERR;
    while(extractFirstEleList(list, &e) == OK) {
        destroyEleList(&e);
	}
    *list = NULL;
    
    return OK;
}

BOOL isEmptyList(const _LIST *list) {
    if(!list || *list)
        return FALSE;
    return TRUE;
}

int numElemsList(const _LIST *list) {
    int i;
    NODE* n;
    if(!list)
        return -1;
    for(n = *list, i = 0; n != NULL; n = n->next, i++);
    return i;
}

BOOL isInList(_LIST* l, const ELELIST* e) {
	NODE* n;
	for(n = *l; n != NULL; n = n->next) {
		if (equal(&(n->data), e)) {
			return TRUE;
		}
	}
	return FALSE;
}

const ELELIST* searchInList(_LIST* l, const ELELIST* e) {
	NODE* n;
	for(n = *l; n != NULL; n = n->next) {
		if (equal(&(n->data), e)) {
			return (const ELELIST*) &(n->data);
		}
	}
	return NULL;
}

STATUS change(_LIST* l, const ELELIST* e) {
	NODE* n;
	for (n = *l; n != NULL; n = n->next) {
		if (equal(&(n->data), e)) {
			return copyEleList(&(n->data), e);
		}
	}
	return ERR;
}

