

/*
 * File:   serverTest.c
 * Author: ana
 *
 * Created on Feb 7, 2016, 10:19:38 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../includes/COclient.h"
#define LOCALHOST 0xC0A80102

/*
 * Simple C Test Suite
 */





void test1() {
    
      char array[400];
 	int desc;
   
    printf("clientTest test 1\n");
    
    desc=initiate_clientCO(AF_INET, 7, LOCALHOST);
  	strcpy(array, "blablabla");
	send_msg(desc, array, 400);
	recv_msg(desc, array, 400);
	printf("%s", array);
    close(desc);
  
    return ;
    
}



int main(int argc, char** argv) {
	
    printf("%%SUITE_STARTING%% clientTest\n");
    printf("%%SUITE_STARTED%%\n");
	
	
		
		test1();
		printf("%%TEST_FINISHED%% time=0 test1 (serverTest) \n");
		
    
    return (EXIT_SUCCESS);
}

