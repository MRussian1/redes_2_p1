#include "../includes/IRC.h"
// nick Undyne
//user aa * * :aa

MSG_EXIT parseMessage (char * question, int desc, ExtraInfo * extrainfo) {
    char * command = NULL;
    char * strret = NULL;
    MSG_EXIT ret = __OK;

    strret = IRC_UnPipelineCommands (question, &command, NULL);
    syslog (LOG_INFO, "EL COMANDO es %s", command);
    if (strret == NULL && command == NULL) {
        return __ERR;
    }
    ret = parseCommandGeneral (command, desc, extrainfo);

    free(command);
    if (ret == __CLOSE)
        return __CLOSE;

    if (strret != NULL) {
        while (strret != NULL && ret != __CLOSE) {
            strret = IRC_UnPipelineCommands (NULL, &command, strret);
            if (strret == NULL && command == NULL) {
                return __ERR;
            }
            ret = parseCommandGeneral(command, desc,extrainfo);

            free(command);
        }
    }
    return ret;
}

MSG_EXIT parseCommandGeneral (char * command, int desc, ExtraInfo * extrainfo) {
    int codigo = __NOMSG;
    char * answer = NULL;

    if (command != NULL && strlen(command) > 1) {
        codigo = parseCommand (command, &answer, extrainfo);
        if (codigo == __OK || codigo == __PING|| codigo == __CLOSE) {
            if ((send_msg(desc, answer, strlen(answer)) == -1)
                    && errno == EPIPE) {
                errno = 0;
                free(answer);
                return __CLOSE;    //Cliente ha cerrado conexión, debemos cerrar por nuestro lado
            }
            free(answer);
        }
    }
    return codigo;
}

MSG_EXIT parseCommand (char * command, char ** answer, ExtraInfo * extrainfo) {

    if (extrainfo->chatting == 1) {
       return parseConnected (command, answer,  extrainfo);
    }
    return parseNotConnected (command, answer, extrainfo);
}

MSG_EXIT parseNotConnected (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    char * prefix = NULL;
    char * password = NULL;
    int i = IRC_CommandQuery (command);
    
    switch (i) {
        case PASS:
            IRCParse_Pass (command, &prefix, &password);
            if (IRC_IsValid (password, PASS_size, NULL, IRC_USER) != IRC_OK) {
                return __NOMSG;
            }
            strcpy(extrainfo->password, password);
            extrainfo->okPass = 1;
            break;
         case NICK:
            return parseNotConnectedNick(command, answer,  extrainfo);
         case USER:
            return parseNotConnectedUser(command, answer,  extrainfo);
        default:
            IRCMsg_ErrNotRegisterd (answer,SERVER_NAME, SERVER_NAME);
     }
    return __OK;
}

MSG_EXIT parseConnected (char * command, char ** answer,
        ExtraInfo * extrainfo) {

    switch (IRC_CommandQuery (command)) {
        /*Parse connected nick*/
        case NICK:
            return parseConnectedNick(command, answer, extrainfo );
        case MODE:
            return parseConnectedMode(command, answer,  extrainfo);
        case QUIT:
            return parseConnectedQuit(command, answer, extrainfo);
        case JOIN:
            return parseConnectedJoin(command, answer,  extrainfo);
        case PART:
             return  parseConnectedPart(command, answer, extrainfo);
        case TOPIC:
            return parseConnectedTopic(command, answer, extrainfo);
        case NAMES:
            return parseConnectedNames(command, answer, extrainfo);
        case LIST:
             return parseConnectedList(command, answer, extrainfo);
        case KICK:
            return parseConnectedKick(command, answer,  extrainfo);
        case PRIVMSG:
            return parseConnectedPrivMsg(command, answer,  extrainfo);
        case MOTD:
            return parseConnectedMotd(command, answer, extrainfo);
        case WHOIS:
           return parseConnectedWhois(command, answer, extrainfo);
        case PING:
           return parseConnectedPing (command, answer,  extrainfo); 
        case PONG:
           return parseConnectedPong (command, answer,  extrainfo);
        case AWAY:
            return parseConnectedAway(command, answer,  extrainfo);
    }
    IRCMsg_ErrUnKnownCommand (answer, SERVER_NAME, extrainfo->nick,
            strtok (command, " \n\r" ));
    
    return __OK;
}

int user_rpl (char ** answer, ExtraInfo* extrainfo) {
    char *ans1, *ans2, *ans3, *ans4, *ans5;
    /*001*/
    IRCMsg_RplWelcome (&ans1, SERVER_NAME, extrainfo->nick,
            extrainfo->nick, extrainfo->user, extrainfo->host);
    /*002*/
    IRCMsg_RplYourHost (&ans2, SERVER_NAME, extrainfo->nick, SERVER_NAME,
            VERSION_NAME);
    /*003*/
    IRCMsg_RplCreated (&ans3, SERVER_NAME, extrainfo->nick);
    /*004*/
    IRCMsg_RplMyInfo(&ans4, SERVER_NAME, extrainfo->nick, SERVER_NAME,
            VERSION_NAME, "abBcCioqrRswx", "abehiIklmMnoOPqQrRstvVz");
    /*005*/
    IRCMsg_RplBounce (&ans5, SERVER_NAME, extrainfo->nick, SERVER_NAME,
            extrainfo->port);
    
    IRC_PipelineCommands (answer, ans1, ans2, ans3, ans4, ans5, NULL);
    /*long IRCTADUser_GetNickList (char ***nicklist, long *nelements)*/
    /*251*//* IRCMsg_RplLuserClient (answer, SERVER_NAME, extrainfo->nick, int nusers, int ninvisibles, 1);*/
    /*254*//* IRCMsg_RplLuserChannels (answer, SERVER_NAME, extrainfo->nick, int nchannels);*/
    /*255*//* IRCMsg_RplLuserMe (answer, SERVER_NAME, extrainfo->nick, int nclients, 1);*/
    // motd_apply(answer,extrainfo);
    free(ans1);
    free(ans2);
    free(ans3);
    free(ans4);
    free(ans5);
    return 0;
}

int motd_apply (char ** answer, ExtraInfo* extrainfo) {
    char *ans1, *ans2, *ans3, *ans4, *ans5, *ans6, *ans7;
    IRCMsg_RplMotdStart (&ans1, SERVER_NAME, extrainfo->nick, extrainfo->nick);
    IRCMsg_RplMotd (&ans2, SERVER_NAME, extrainfo->nick,
            "El oro, la fama, el poder ");
    IRCMsg_RplMotd (&ans3, SERVER_NAME, extrainfo->nick,
            "todo lo tuvo el hombre que en su dia se autoproclamo el rey de \
los piratas, Gold Roger ");
    IRCMsg_RplMotd (&ans4, SERVER_NAME, extrainfo->nick, "Mas sus ultimas \
palabras no fueron muy afortunadas ");
    IRCMsg_RplMotd (&ans5, SERVER_NAME, extrainfo->nick, "¿Mi tesoro? Lo deje \
todo ahi, buscadlo si quereis, ojala se le atragante al rufian que lo \
encuentre");
    IRCMsg_RplMotd (&ans6, SERVER_NAME, extrainfo->nick, "Y todos los lobos de \
los 7 mares zarparon rumbo a la Grand Line y asi comenzo la gran batida");
    IRCMsg_RplEndOfMotd (&ans7, SERVER_NAME, extrainfo->nick);
    
    IRC_PipelineCommands (answer, ans1, ans2, ans3, NULL);

    if ((send_msg(extrainfo->desc, *answer, strlen(*answer)) == -1)
            && errno == EPIPE) {
        errno = 0;
        free(answer);
        return __CLOSE;    //Cliente ha cerrado conexión, debemos cerrar por nuestro lado
    }
    free(*answer);
    IRC_PipelineCommands (answer, ans4, ans5,ans6, ans7, NULL);
    free(ans1);
    free(ans2);
    free(ans3);
    free(ans4);
    free(ans5);
    free(ans6);
    free(ans7);
    return 0;
}

int whois_rpl (char ** answer, ExtraInfo* extrainfo, char* target) {
    char *ans1 = NULL;
    char *ans2 = NULL;
    char *ans3 = NULL;
    char *ans4 = NULL;
    char *ans5 = NULL;
    char *ansaway = NULL;
    char *msg = NULL;
    char ** listchan = NULL;
    long num;
    int i;

    /*311*/
    IRCMsg_RplWhoIsUser (&ans1, SERVER_NAME, extrainfo->nick, target,
            IRCTADUser_GetUserByNick (extrainfo->nick),
            IRCTADUser_GetHostByNick (extrainfo->nick) ,
            IRCTADUser_GetRealnameByNick (extrainfo->nick));
    /*312*/
    IRCMsg_RplWhoIsServer (&ans2, SERVER_NAME, extrainfo->nick, target,
            SERVER_NAME, SERVER_DESC);
    IRCTAD_ListChannelsOfUser (IRCTADUser_GetUserByNick (extrainfo->nick),
            &listchan , &num);
    chanlist (&ans4, listchan, num, extrainfo->nick);
    for (i = 0; i < num; i++) {
        free(listchan[i]);
    }
    free(listchan);
    /*319*/
    IRCMsg_RplWhoIsChannels (&ans3, SERVER_NAME, extrainfo->nick, target, ans4,
            NULL);
    
    msg = IRCTAD_GetAway (IRCTADUser_GetUserByNick (extrainfo->nick));
    if (msg != NULL) {
        IRCMsg_RplAway (&ansaway, SERVER_NAME, extrainfo->nick,
                extrainfo->nick, msg);
    }

    /*318*/
    IRCMsg_RplEndOfWhoIs (&ans5, SERVER_NAME, extrainfo->nick, target);
    
    if (msg != NULL) {
        IRC_PipelineCommands (answer, ans1, ans2, ans3, ansaway, ans5, NULL);
        free(ansaway);
    } else {
        IRC_PipelineCommands (answer, ans1, ans2, ans3, ans5, NULL);
    }
    free(ans1);
    free(ans2);
    free(ans3);
    free(ans4);
    free(ans5);
    return 0;
}

int join_rpl (char ** answer, ExtraInfo * extrainfo, char * channel,
        char * key) {
    char *ans1 = NULL;
    char *ans2 = NULL;
    char *ans3 = NULL;
    char *ans4 = NULL;
    char *topic = NULL;
    long codSalida = 0;
    char **listusers;
    long num;
    int i;
    char *ans;
    time_t t;

   
    if (IRCTADChan_GetNumberOfUsers (channel) == IRCERR_INVALIDCHANNELNAME) {
        codSalida = IRCTAD_JoinChannel (channel, extrainfo->user , "ov", key);
    } else {
        codSalida = IRCTAD_JoinChannel (channel, extrainfo->user, "rv", key);
    }
    //IRCMsg_ErrBadChannelKey (char **command, char *prefix, char *nick, char *channeñ)
    switch (codSalida) {
        case IRCERR_NOVALIDUSER:
            IRCMsg_ErrNoSuchNick (answer, SERVER_NAME, extrainfo->nick,
                    extrainfo->nick);
            return __OK;
        case IRCERR_NOVALIDCHANNEL:
            IRCMsg_ErrNoSuchChannel (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            return __OK;
        case IRCERR_USERSLIMITEXCEEDED:
            IRCMsg_ErrChannelIsFull (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            return __OK;
        case IRCERR_NOENOUGHMEMORY:
            return __NOMSG;
        case IRCERR_INVALIDCHANNELNAME:
            IRCMsg_ErrNoSuchChannel (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            return __OK;
        case IRCERR_NAMEINUSE:
            return __NOMSG;
        case IRCERR_BANEDUSERONCHANNEL:
            IRCMsg_ErrBannedFromChan (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            return __OK;
        case IRCERR_NOINVITEDUSER:
            return __NOMSG;   
        case IRCERR_FAIL:
            IRCMsg_ErrBadChannelKey (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            return __OK;
    }
    
    /*Mensaje a otra gentuza*/
    IRCMsg_Join (&ans1, extrainfo->nick, NULL, NULL, channel);
    if (msgToChan(ans1,  extrainfo, channel) == 0) {

        t = time(NULL);
        topic = IRCTADChan_GetTopic (channel, &t);
        if (topic != NULL) {
           IRCMsg_RplTopic (&ans2, SERVER_NAME, extrainfo->nick, channel, topic);
        }
        IRCTAD_ListUsersOnChannel (channel, &listusers, &num);
        oplist(&ans, listusers, num, channel);
        for (i = 0; i < num; i++) {
            free(listusers[i]);
        }
        free(listusers);
        if (IRCTADChan_GetMode (channel) & IRCMODE_SECRET) {
        
            IRCMsg_RplNamReply (&ans3, SERVER_NAME, extrainfo->nick, "@",
                    channel, ans);
        } else if (IRCTADChan_GetMode (channel)    & IRCMODE_PRIVATE) {
                
            IRCMsg_RplNamReply (&ans3, SERVER_NAME, extrainfo->nick, "*",
                    channel, ans);
        } else {
            IRCMsg_RplNamReply (&ans3, SERVER_NAME, extrainfo->nick, "=",
                    channel, ans);
        }
        free(ans);
        IRCMsg_RplEndOfNames (&ans4, SERVER_NAME, extrainfo->nick, channel);
        if (topic != NULL) {
            IRC_PipelineCommands (answer, ans1, ans2, ans3, ans4, NULL);
            free(ans2);
        } else {
            IRC_PipelineCommands (answer, ans1, ans3, ans4, NULL);
        }
        free(ans1);
        free(ans3);
        free(ans4);
    }
    return __OK;
}

int list_rpltodo(char ** answer, ExtraInfo* extrainfo, char * mask){
    char ** val = NULL;
    char * ans2 = NULL;
    char *topic = NULL;
    long num, i;
    time_t t;
    IRCTADChan_GetList (&val, &num, mask);

    for (i = 0; i < num; i++) {
        t = time(NULL);
        if (!(IRCTADChan_GetMode (val[i]) & IRCMODE_SECRET)) {
            topic = IRCTADChan_GetTopic (val[i], &t);
        
            if (topic == NULL) {
                IRCMsg_RplList (&ans2, SERVER_NAME, extrainfo->nick,
                        val[i],  "", "");
            } else {
                IRCMsg_RplList (&ans2, SERVER_NAME, extrainfo->nick,
                        val[i],  "", topic);
            }
        
            if ((send_msg (extrainfo->desc, ans2, strlen(ans2)) == -1)
                    && errno == EPIPE) {
                errno = 0;
                free(answer);
                return __CLOSE;    //Cliente ha cerrado conexión, debemos cerrar por nuestro lado
            }
            free(ans2);
        }
    }
    IRCMsg_RplListEnd (answer, SERVER_NAME, extrainfo->nick);
    IRCTADChan_FreeList (val, num);
    return 0;
}

int names_rpltodo (char ** answer, ExtraInfo* extrainfo, char * channel) {
    char **listusers, **listchan;
    long num = 0, numchan = 0, i, j;
    char *ans = NULL, *ans1 = NULL, *ans2 = NULL, *ans3 = NULL;
    
    if (channel == NULL) {
        IRCTADChan_GetList (&listchan, &numchan, NULL);
        for (i = 0; i < numchan; i++) {

            IRCTAD_ListUsersOnChannel (listchan[i], &listusers, &num);
            oplist(&ans, listusers, num, listchan[i]);
            for (j = 0; j < num; j++) {
                free(listusers[j]);
            }
            free(listusers);
            if (IRCTADChan_GetMode (listchan[i]) & IRCMODE_SECRET) {
                IRCMsg_RplNamReply (&ans2, SERVER_NAME,
                        extrainfo->nick, "@", listchan[i], ans);
            } else if (IRCTADChan_GetMode (listchan[i])    & IRCMODE_PRIVATE) {
                IRCMsg_RplNamReply (&ans2, SERVER_NAME,
                        extrainfo->nick, "*", listchan[i], ans);
            } else {
                IRCMsg_RplNamReply (&ans2, SERVER_NAME,
                        extrainfo->nick, "=", listchan[i], ans);
            }
            free(ans);
            if (ans1 != NULL) {
                IRC_PipelineCommands (&ans3, ans1, ans2, NULL);
                free(ans1);
                free(ans2);
                ans1 = ans3;
                ans3 = NULL;
            } else {
                ans1 = ans2;
                ans2 = NULL;
            }
            //IRCTAD_FreeListUsersOnChannel (listusers, num);
        }
        IRCTADChan_FreeList (listchan, numchan);
    } else {
        IRCTAD_ListUsersOnChannel (channel, &listusers, &num);
        if (num > 0) {
            oplist(&ans1, listusers, num, channel);
            for (i = 0; i < num; i++) {
                free(listusers[i]);
            }
            free(listusers);
            if (IRCTADChan_GetMode (channel) & IRCMODE_SECRET) {
                IRCMsg_RplNamReply (&ans3, SERVER_NAME,
                        extrainfo->nick, "@", channel, ans1);
            } else if (IRCTADChan_GetMode (channel) & IRCMODE_PRIVATE) {
                IRCMsg_RplNamReply (&ans3, SERVER_NAME,
                        extrainfo->nick, "*", channel, ans1);
            } else {
                IRCMsg_RplNamReply (&ans3, SERVER_NAME,
                        extrainfo->nick, "=", channel, ans1);
            }
            
            free(ans1);
            ans1 = ans3;
            ans3 = NULL;
        }
    }

    IRCMsg_RplEndOfNames (&ans3, SERVER_NAME, extrainfo->nick, (channel?channel:""));
    if (num > 0) {
        IRC_PipelineCommands (answer, ans1, ans3, NULL);
        free(ans3);
        free(ans1);
    } else {
        *answer = ans3;
    }
    return 0;
}


int oplist (char ** ans, char ** list, long num, char * channel) {
    long i, codObtained;
    char help[500];
    *help = '\0';
    for (i = 0; i < num; i++) {
        codObtained = IRCTAD_GetUserModeOnChannel (channel, list[i]);
        if ((codObtained & IRCUMODE_CREATOR)
                || (codObtained & IRCUMODE_OPERATOR)
                || (codObtained & IRCUMODE_LOCALOPERATOR)) {
            sprintf(help, "%s @%s", help, IRCTADUser_GetNickByUser(list[i]));
        } else {
            sprintf(help, "%s %s", help, IRCTADUser_GetNickByUser(list[i]));
        }
    }

    *ans = (char*) malloc(sizeof(char) * strlen(help) + 5);
    strcpy(*ans, help);
    return 0;
}

int chanlist (char ** ans, char ** list, long num, char * nick) {
    long i, codObtained;
    char help[1000];
    *help = '\0';
    if (num > 0) {
        codObtained = IRCTAD_GetUserModeOnChannel (list[0], nick);
        if ((codObtained & IRCUMODE_CREATOR)
                || (codObtained & IRCUMODE_OPERATOR)
                || (codObtained & IRCUMODE_LOCALOPERATOR)) {
            sprintf(help, "@%s", list[0]);
        } else {
            sprintf(help, "%s", list[0]);
        }
        for (i = 1; i < num; i++) {
            codObtained = IRCTAD_GetUserModeOnChannel (list[i], nick);
            if ((codObtained & IRCUMODE_CREATOR)
                    || (codObtained & IRCUMODE_OPERATOR)
                    || (codObtained & IRCUMODE_LOCALOPERATOR)) {
                sprintf(help, "%s @%s", help, list[i]);
            } else {
                sprintf(help, "%s %s", help, list[i]);
            }
        }
    }
    *ans = (char*) malloc (sizeof(char) * strlen(help) + 5);
    strcpy(*ans, help);
    return 0;
}



MSG_EXIT parseNotConnectedNick (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    long codSalida;
    char * prefix = NULL;
    char * msg = NULL;
    char * nick = NULL;
    
    /*Parse not connected nick*/
    codSalida = IRCParse_Nick (command, &prefix, &nick, &msg);

    if (codSalida == IRCERR_ERRONEUSCOMMAND) {
        
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, "", command);
        return __OK;
    }
   /* if(IRC_IsValid (nick, NICK_size, NULL, IRC_KEY) !=IRC_OK){
       IRCMsg_ErrErroneusNickName (answer, SERVER_NAME, nick, nick);
             free(prefix);
            free(nick);
            return __OK;
    }*/

    if (IRCTADUser_GetUserByNick (nick) != NULL) {
        IRCMsg_ErrNickNameInUse (answer, SERVER_NAME,
                extrainfo->nick, nick);
        free(prefix);
        free(nick);

        return __OK;
    }
    strcpy(extrainfo->nick,nick);
    free(prefix);
    free(nick);

    extrainfo->okNick = 1;
    return __NOMSG;
}

MSG_EXIT parseConnectedNick (char * command, char ** answer,
        ExtraInfo * extrainfo) {

    long codSalida;
    char *prefix, *nick, *msg, *user;
    codSalida = IRCParse_Nick (command, &prefix, &nick, &msg);
    if (codSalida != IRC_OK) {
        
        IRCMsg_ErrNoNickNameGiven (answer, SERVER_NAME, extrainfo->nick);
        return __OK;
    }

    /* if(IRC_IsValid (nick, NICK_size, NULL, IRC_USER) !=IRC_OK){
        IRCMsg_ErrErroneusNickName (answer, SERVER_NAME, nick, nick);
        free(prefix);
        free(nick);
        return __OK;
    }*/
    user = IRCTADUser_GetUserByNick (extrainfo->nick);
    if (user == NULL) {
        IRCMsg_ErrErroneusNickName (answer, SERVER_NAME,
                extrainfo->nick, nick);
    } else if (IRCTADUser_SetNickByUser (nick, user) != IRC_OK) {
        IRCMsg_ErrNickNameInUse (answer, SERVER_NAME,
                extrainfo->nick, nick);
    } else {
        strcpy(extrainfo->nick, nick);
        extrainfo->okNick = 1;
        IRCMsg_Nick (answer, SERVER_NAME, NULL, nick);
    }
    
    free(prefix);
    free(nick);
    free(msg);
    return __OK;
}

MSG_EXIT parseConnectedTopic (char * command, char ** answer,
        ExtraInfo * extrainfo ) {
    char *ans1, *ans2;
    char *topic, *channel, *prefix;
    long codSalida, codObtained;

    codSalida = IRCParse_Topic (command, &prefix, &channel, &topic);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME,
                extrainfo->nick, command);
        
        return __OK;
    }
    if (IRCTAD_TestUserOnChannel (channel,
            IRCTADUser_GetUserByNick (extrainfo->nick)) != IRC_OK) {
        IRCMsg_ErrNotOnChannel (answer, SERVER_NAME, extrainfo->nick,
                "", channel);
        free(prefix);
        free(channel);
        free(topic);
        
        return __OK;
    }
    codObtained = IRCTAD_GetUserModeOnChannel (channel,
            IRCTADUser_GetUserByNick (extrainfo->nick));
    if ((IRCTADChan_GetMode (channel) & IRCMODE_TOPICOP)
            && !(codObtained & IRCUMODE_OPERATOR)) {
        IRCMsg_ErrChanOPrivsNeeded (answer, SERVER_NAME, extrainfo->nick,
                channel);
        free(prefix);
        free(channel);
        free(topic);
        
        return __OK;
    }

    if (topic == NULL) {
        IRCMsg_RplNoTopic (answer, SERVER_NAME, extrainfo->nick, channel,
            "NO TOPIC");
        free(prefix);
        free(channel);
        return __OK;
    }
    
    codSalida = IRCTADChan_SetTopic (channel, topic);
    if (codSalida != IRC_OK) {
        IRCMsg_RplNoTopic (answer, SERVER_NAME, extrainfo->nick,
                channel, "NO TOPIC");
        free(prefix);
        free(channel);
        return __OK;
    }
    
    IRCMsg_Topic (&ans1, SERVER_NAME, channel, topic);
    IRCMsg_RplTopic (&ans2, SERVER_NAME, extrainfo->nick,
            channel, topic);
    IRC_PipelineCommands (answer, ans1, ans2, NULL);

    free(ans1);
    free(ans2);
    free(prefix);
    free(channel);
    free(topic);
    return __OK;
}

MSG_EXIT parseConnectedPart (char * command, char ** answer,
        ExtraInfo * extrainfo) {

    char * prefix = NULL;
    char *channel = NULL;
    char *msg = NULL;
    long codSalida;
    
    codSalida = IRCParse_Part (command, &prefix, &channel, &msg);

    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME,
                extrainfo->nick, command);
        
        return __OK;
    }
    
    codSalida = IRCTAD_PartChannel (channel,
            IRCTADUser_GetUserByNick (extrainfo->nick));
    switch (codSalida) {
        case IRCERR_NOVALIDCHANNEL:
            IRCMsg_ErrNoSuchChannel (answer, SERVER_NAME, extrainfo->nick,
                    channel);
            
            free(prefix);
            free(channel);
            free(msg);
            return __OK;
        case IRCERR_NOVALIDUSER:
            IRCMsg_ErrUserNotInChannel (answer, SERVER_NAME, extrainfo->nick,
                    extrainfo->nick, "");
            
            free(prefix);
            free(channel);
            free(msg);
            return __OK;
        default:
            break;
    }
    free(prefix);
    
    IRC_Prefix (&prefix, extrainfo->nick,
            IRCTADUser_GetUserByNick (extrainfo->nick),
                    extrainfo->host, SERVER_NAME);
    IRCMsg_Part (answer, prefix , channel, msg);
    msgToChan(*answer, extrainfo, channel);

    free(prefix);
    free(channel);
    free(msg);

    return __OK;
}

MSG_EXIT parseConnectedKick(char * command, char ** answer,
        ExtraInfo * extrainfo) {

    char *prefix = NULL;
    char *channel = NULL;
    char *user = NULL;
    char *comment = NULL;
    long codSalida;
    
    codSalida = IRCParse_Kick (command, &prefix, &channel, &user, &comment);

    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick,
                command);

        return __OK;
    }

    codSalida = IRCTAD_GetUserModeOnChannel (channel,
            IRCTADUser_GetUserByNick (extrainfo->nick));
    if ((codSalida > 0) && !(codSalida & (IRCUMODE_CREATOR
            | IRCUMODE_OPERATOR | IRCUMODE_LOCALOPERATOR))) {

        IRCMsg_ErrChanOPrivsNeeded (answer, SERVER_NAME,
                extrainfo->nick, channel);

        free(user);
        free(comment);
        free(channel);
        free(prefix);
        return __OK;
    }

    codSalida = IRCTAD_KickUserFromChannel (channel, user);
    if (codSalida == IRCERR_NOVALIDUSER) {
        IRCMsg_ErrUserNotInChannel (answer, SERVER_NAME,
                extrainfo->nick, extrainfo->nick, channel);

        free(user);
        free(comment);
        free(channel);
        free(prefix);
        return __OK;
    } else if (codSalida == IRCERR_NOVALIDCHANNEL) {
        IRCMsg_ErrNoSuchChannel (answer, prefix, extrainfo->nick,
                channel);

        free(user);
        free(comment);
        free(channel);
        free(prefix);
        return __OK;
    }
    
    codSalida = IRCMsg_Kick (answer, extrainfo->nick, channel, user,
            comment);
    if (codSalida == IRCERR_COMMANDTOOLONG) {
        syslog(LOG_NOTICE, "Command too long %s\n", *answer);
        return __NOMSG;
    }

    msgToChan(*answer, extrainfo, channel);
    int desc = getFDByUsername(user);
    if (desc < 0) {
        syslog(LOG_ERR, "File descriptor for user '%s' not found", user);
    } else {
        if ((send_msg(desc, *answer, strlen(*answer)) == -1)
                && errno == EPIPE) {
            //ignorar error, se cerrara por falta de pong
            errno = 0;
        }
    }
    
    free(prefix);
    free(channel);
    free(comment);
    free(user);
    return __OK;
}

MSG_EXIT parseNotConnectedUser (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    long codSalida;
    char *prefix, *realname, *serverother, *modehost, *user;

    if (IRCParse_User (command, &prefix, &user, &modehost,
            &serverother, &realname) == IRCERR_ERRONEUSCOMMAND) {

         IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick,
                command);
         return __OK;
    }
    if (extrainfo->okNick != 1) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick,
                command);

        free(prefix);
        free(user);
        free(modehost);
        free(serverother);
        free(realname);
        return __OK;

    }
    if (extrainfo->okPass == 1) {
        codSalida = IRCTADUser_Add (user, extrainfo->nick, realname,
                extrainfo->password, extrainfo->host, extrainfo->ip);
    } else {
        codSalida = IRCTADUser_Add (user, extrainfo->nick, realname,
                NULL, extrainfo->host, extrainfo->ip);
    }
    if (codSalida != IRC_OK) {
        IRCMsg_ErrAlreadyRegistred (answer, SERVER_NAME, extrainfo->nick);

        free(prefix);
        free(user);
        free(modehost);
        free(serverother);
        free(realname);
        return __OK;
    }
    strcpy(extrainfo->user, user);
    extrainfo->chatting = 1;
    tellUsername(extrainfo->desc, user);
    user_rpl(answer, extrainfo);

    free(prefix);
    free(user);
    free(modehost);
    free(serverother);
    free(realname);
    return __OK;
}

MSG_EXIT parseConnectedPrivMsg (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    char *prefix = NULL, *nick = NULL, *msg;
    long codSalida;
    
    codSalida = IRCParse_Privmsg (command, &prefix, &nick, &msg);
    if (codSalida == IRCERR_ERRONEUSCOMMAND) {
        IRCMsg_ErrNoRecipient(answer, SERVER_NAME, extrainfo->nick, command);

        return __OK;
    }
    //Must contain at least one '.' and no '[*?]' after the last one
    char* n = strrchr(nick, '.');    //has a '.'
    if (n != NULL) {
        if (strchr(n, '*') || strchr(n, '?')) {
            IRCMsg_ErrWildTopLevel (answer, SERVER_NAME, extrainfo->nick, "");

            free(nick);
            free(prefix);
            return __OK;
        }
    }
    if (*msg == '\0') {
        IRCMsg_ErrNoTextToSend(answer, SERVER_NAME, extrainfo->nick);

        free(nick);
        free(prefix);
        free(msg);
        return __OK;
    }
    char* ansmsg = NULL;
    IRCMsg_Privmsg (&ansmsg, extrainfo->nick, nick, msg);
    free(msg);
    
    if (nick[0] == '+'|| nick[0] == '#') {
        
        int cod = msgToChan(ansmsg, extrainfo, nick);
        free(ansmsg);
        free(prefix);
        
        if (cod == -1) {
            IRCMsg_ErrNoSuchChannel(answer, SERVER_NAME, extrainfo->nick, nick);
            free(nick);
            return __OK;
        } else {
            free(nick);
            return __NOMSG;
        }
    }
    
    char* user = IRCTADUser_GetUserByNick(nick);
    if (user == NULL) {
        IRCMsg_ErrNoSuchNick (answer, SERVER_NAME, extrainfo->nick, nick);
        free(nick);
        free(prefix);
        free(ansmsg);
        return __OK;
    }
    char* msg1 = IRCTAD_GetAway (nick);
    if (msg1 != NULL) {
        IRCMsg_RplAway (answer, SERVER_NAME, extrainfo->nick, extrainfo->nick,
                msg1);
        free(nick);
        free(prefix);
        free(ansmsg);
        return __OK;
    }
    int usr_fd = getFDByUsername(user);
    if (usr_fd < 0) {
        syslog(LOG_ERR, "File descriptor for user '%s' not found", nick);
    } else {
        if ((send_msg(usr_fd, ansmsg, strlen(ansmsg)) == -1)
                && errno == EPIPE) {
            //ignorar y dejar que falta de PONG cierre la conexión
            errno = 0;
        }
    }
    free(ansmsg);
    free(nick);
    free(prefix);
    return __NOMSG;
}

int msgToChan (char * msgtosend,  ExtraInfo *extrainfo, char *channel) {
    long num_users;
    char ** listusers = NULL;
    long i;
    int usr_fd;
    
    long codSalida = IRCTAD_ListUsersOnChannel (channel, &listusers,
            &num_users);
    if (codSalida == IRCERR_NOVALIDCHANNEL) {
        return -1;
    }
    for (i = 0; i < num_users; i++) {
        if (listusers[i] == NULL) {
            continue;
        } else if (strcmp(listusers[i],
                IRCTADUser_GetUserByNick (extrainfo->nick)) == 0) {
            free (listusers[i]);
            continue;    //send to all but the sender
        }
        usr_fd = getFDByUsername(listusers[i]);
        if (usr_fd < 0) {
            syslog(LOG_ERR, "File descriptor for user '%s' not found",
                    listusers[i]);
            free (listusers[i]);
            continue;
        }
        if ((send_msg(usr_fd, msgtosend, strlen(msgtosend)) == -1)
                && errno == EPIPE) {
            //ignorar y dejar que falta de PONG cierre la conexión
            errno = 0;
        }
        free (listusers[i]);
    }
    free(listusers);
    return 0;
}

MSG_EXIT parseConnectedPong (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    /*char *prefix, *server, *server2, *received;
    long codSalida;

    codSalida = IRCParse_Pong (command, &prefix, &server, &server2, &received);
    if (codSalida != IRC_OK) {
         IRCMsg_ErrNoOrigin (answer, SERVER_NAME, extrainfo->nick, "PONG");
         
         return __OK;
    }
    free(prefix);
    free(server);
    free(server2);
    free(received);*/
    return __PONG;
}

MSG_EXIT parseConnectedPing (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    char* prefix = NULL;
    char* server = NULL;
    char* server2 = NULL;
    char* msg = NULL;
    long codSalida;
    
    codSalida = IRCParse_Ping (command, &prefix, &server, &server2, &msg);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNoOrigin (answer, SERVER_NAME, extrainfo->nick, command);

        return __OK;
    }
   /* if(server2==NULL){
        IRCMsg_Pong (answer, SERVER_NAME, SERVER_NAME , server);
    }else{
        IRCMsg_Pong (answer, SERVER_NAME, server2 , server);

    }*/
    IRCMsg_Pong (answer, SERVER_NAME, SERVER_NAME, NULL, server);
    
    if (prefix != NULL) {
        free(prefix);
    }
    if (server != NULL) {
        free(server);
    }
    if (server2 != NULL) {
        free(server2);
    }
    if (msg != NULL) {
        free(msg);
    }
    return __PING;
}  

MSG_EXIT parseConnectedJoin(char * command, char ** answer,
        ExtraInfo * extrainfo) {
    char *prefix = NULL;
    char *channel = NULL;
    char *key = NULL;
    char *msg = NULL;
    long codSalida;
    codSalida = IRCParse_Join (command, &prefix, &channel, &key, &msg);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick, "JOIN");

        return __OK;
    }
    join_rpl(answer, extrainfo, channel, key);

    free(prefix);
    free(channel);
    free(key);
    free(msg);
    return __OK;
}

MSG_EXIT parseConnectedMode (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    char *prefix = NULL;
    char *channeluser = NULL;
    char *mode = NULL;
    char *user = NULL;
    long codObtained;
    long codSalida = IRCParse_Mode (command, &prefix, &channeluser, &mode,
            &user);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick, "MODE");
        
        return __OK;
    }
    if (channeluser[0] == '#' || channeluser[0] == '+') {
        // IRCTAD_TestUserOnChannel (char *channel, char *user)
        codObtained = IRCTAD_GetUserModeOnChannel (channeluser,
                IRCTADUser_GetUserByNick (extrainfo->nick));
        if (codObtained < 0) {
            
            IRCMsg_ErrNoSuchNick (answer, SERVER_NAME, extrainfo->nick,
                    channeluser);
            free(user);
            free(mode);
            free(channeluser);
            free(prefix);
            return __OK;
        }
        
        if (!((codObtained & IRCUMODE_CREATOR )
                || (codObtained & IRCUMODE_OPERATOR)
                || (codObtained & IRCUMODE_LOCALOPERATOR))) {
            
            IRCMsg_ErrChanOPrivsNeeded (answer, SERVER_NAME, extrainfo->nick,
                    channeluser);
            free(user);
            free(mode);
            free(channeluser);
            free(prefix);
            return __OK;
        }
        if (mode == NULL) {
            codSalida = mode_getMode (answer, extrainfo, channeluser);
        } else if (strstr(mode, "k") != NULL) {
            IRCTADChan_SetPassword (channeluser, user);
            codSalida = mode_channel(answer, extrainfo, channeluser,  mode);
        } else if (user == NULL) {
            codSalida = mode_channel (answer, extrainfo, channeluser,  mode);
        } else {
            codSalida = mode_userOnChannel (answer, extrainfo, channeluser,
                    user, mode);
        }
    } else {
        codSalida = mode_user (answer, extrainfo, channeluser,  mode);
    }
    
    free(user);
    free(mode);
    free(channeluser);
    free(prefix);
    return codSalida;

}

MSG_EXIT mode_channel(char **answer, ExtraInfo * extrainfo, char * channel,
        char * mode) {

    if (strstr (mode, "-" ) == NULL) {
        IRCTADChan_AddMode (channel, mode);
    } else {
        IRCTADChan_DeleteMode (channel, mode);
    }
    IRCMsg_Mode (answer, extrainfo->nick, channel, mode, NULL);
    return __OK;
    
}

MSG_EXIT mode_user (char **answer, ExtraInfo * extrainfo, char * user,
        char * mode) {
    IRCMsg_Mode (answer, extrainfo->nick, user, mode, NULL);
    return __OK;
}

MSG_EXIT mode_userOnChannel (char **answer, ExtraInfo * extrainfo,
        char * channel, char * user, char * mode) {
    
    if (strstr (mode, "-" ) == NULL) {
        IRCTAD_AddUserModeOnChannel (channel, user, mode);
    } else {
        IRCTAD_DelUserModeOnChannel (channel, user, mode);
    }
    IRCMsg_Mode (answer, extrainfo->nick, channel, mode, user);
    return __OK;
}

MSG_EXIT mode_getMode (char **answer, ExtraInfo * extrainfo, char * channel) {
    long codSalida = IRCTADChan_GetMode (channel);
    char mode[200];
    
    sprintf(mode, "+");
    if (codSalida & IRCMODE_ANONYMOUS) {
        sprintf(mode, "%sa", mode);
    }
    if (codSalida & IRCMODE_INVITEONLY) {
        sprintf(mode, "%si", mode);
    }
    if (codSalida & IRCMODE_MODERATED) {
        sprintf(mode, "%sm", mode);
    }
    if (codSalida & IRCMODE_NOOUTMESSAGES) {
        sprintf(mode, "%sn", mode);
    }
    if (codSalida & IRCMODE_QUIET) {
        sprintf(mode, "%sq", mode);
    }
    if (codSalida & IRCMODE_PRIVATE) {
        sprintf(mode, "%sp", mode);
    }
    if (codSalida & IRCMODE_SECRET) {
        sprintf(mode, "%ss", mode);
    }
    if (codSalida & IRCMODE_REOP) {
        sprintf(mode, "%sr", mode);
    }
    if (codSalida & IRCMODE_TOPICOP) {
        sprintf(mode, "%st", mode);
    }

    IRCMsg_RplChannelModeIs (answer, SERVER_NAME, extrainfo->nick, channel,
            mode);
    return __OK;
}

MSG_EXIT parseConnectedAway (char * command, char ** answer,
        ExtraInfo * extrainfo) {
    
    char * prefix = NULL;
    char *msg = NULL;
    IRCParse_Away (command, &prefix, &msg);
    if (msg == NULL) {
        IRCTAD_DeleteAway (IRCTADUser_GetUserByNick (extrainfo->nick));
        IRCMsg_RplUnaway (answer, SERVER_NAME, extrainfo->nick);
    } else {
        IRCTAD_SetAway (IRCTADUser_GetUserByNick (extrainfo->nick), msg);
        IRCMsg_RplNowAway (answer, SERVER_NAME, extrainfo->nick);
        free(msg);
    }
    
    free(prefix);
    return __OK;
}

MSG_EXIT parseConnectedQuit(char * command, char ** answer,
        ExtraInfo * extrainfo){
    char * prefix = NULL;
    char *msg = NULL;
    char *ans = NULL;
    char ** listchan = NULL;
    long numchan, i;
    IRCParse_Quit (command, &prefix, &msg);
    IRCTAD_Quit (IRCTADUser_GetUserByNick (extrainfo->nick));

    IRCMsg_Quit (&ans, extrainfo->nick , msg);
    IRCTAD_ListChannelsOfUser (extrainfo->nick, &listchan, &numchan);
    for(i=0; i<numchan; i++){
        msgToChan(ans,  extrainfo, listchan[i]);

    }
    
    
    free(ans);
    IRCMsg_Notice (answer, SERVER_NAME, extrainfo->nick,
                    "Yohoho!");
            
    //respuesta ERROR: closing connection
    free(prefix);
    free(msg);
     return __CLOSE;


}

MSG_EXIT parseConnectedWhois (char * command, char ** answer,
        ExtraInfo * extrainfo) {

    char * prefix = NULL;
    char * target = NULL;
    char *maskarray = NULL;
    long codSalida;

    codSalida = IRCParse_Whois (command, &prefix, &target, &maskarray);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNoNickNameGiven (answer, SERVER_NAME,
            extrainfo->nick);

        return __OK;
    }

    whois_rpl(answer, extrainfo, maskarray);

    free(prefix);
    free(target);
    free(maskarray);
    return __OK;
}

MSG_EXIT parseConnectedMotd (char *command, char ** answer,
        ExtraInfo * extrainfo) {
    char *prefix, *target;
    IRCParse_Motd (command, &prefix, &target);
    if (target != NULL && strcmp(target, SERVER_NAME)) {
        IRCMsg_ErrNoSuchServer (answer, SERVER_NAME, extrainfo->nick,
            target);
    }
    motd_apply (answer, extrainfo);
    free (prefix);
    free (target);
            
    return __OK;
}

MSG_EXIT parseConnectedList (char* command, char ** answer,
        ExtraInfo * extrainfo) {
    char * prefix = NULL;
    char *channel = NULL;
    char *target = NULL;
    long codSalida;
    codSalida = IRCParse_List (command, &prefix, &channel, &target);
    if (codSalida != IRC_OK) {
        IRCMsg_ErrNeedMoreParams (answer, SERVER_NAME, extrainfo->nick,
            command);

        return __OK;
    }
    if (channel == NULL) {
        list_rpltodo(answer, extrainfo, target);
    }
             
    return __OK;
}

MSG_EXIT parseConnectedNames(char * command, char ** answer,
        ExtraInfo * extrainfo){
    char * prefix = NULL;
    char *channel = NULL;
    char *target = NULL;
    IRCParse_Names (command, &prefix, &channel, &target);
    names_rpltodo (answer, extrainfo, channel);
    free(prefix);
    free(channel);
    free(target);
    return __OK;
}
