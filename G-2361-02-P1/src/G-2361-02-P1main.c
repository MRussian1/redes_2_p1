#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <signal.h>
#include <inttypes.h>
#include <strings.h>
#include <errno.h>
#include "../includes/COserver.h"
#include "../includes/thread_module.h"
#include "../includes/types.h"

int server_init_fd;

#define LISTEN_PORT 6667
#define MAX_CONNECTIONS 20
#define PING_PERIOD 30

//mediante process lee los mensajes del cliente tras el accept
STATUS launch_service(void* info) {
	if (info == NULL) {
		return ERR;
	}
	return spawn_thread(*((int*)info));
}

int main(int argc, char** argv) {
	
	long port = LISTEN_PORT;
	if (argc > 1) {
		port = strtol(argv[1], NULL, 10);
		if (port < 0) {
			puts("Puerto inasignable, usando por defecto");
			port = LISTEN_PORT;
		} 
	}
	go_daemon ();
	if (setup_master(PING_PERIOD) == ERR) {
		syslog(LOG_EMERG, "Error al comenzar hilo maestro");
		return EXIT_FAILURE;
	}
	
    server_init_fd = initiate_serverCO(AF_INET, port, MAX_CONNECTIONS);
    int conn_fd;
    if (server_init_fd > -1) {
		while(1) {
			conn_fd = accept_connectionCO(server_init_fd);
			if (errno == EINTR) {
				errno = 0;
				continue;
			} if (conn_fd == -1)
				clean_end(0);
			launch_service(&conn_fd);
		}
	} else {
		syslog(LOG_ERR, "Al iniciar servidor: %s", strerror(errno));
	}
	return 0;
}
