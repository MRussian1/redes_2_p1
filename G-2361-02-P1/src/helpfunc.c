
#include "../includes/IRC.h"

/**
 * 
 *
 */

ExtraInfo * extraInfoCreate (int desc) {
	struct sockaddr_in Direccion;
	struct hostent * host = NULL;
	socklen_t len = sizeof(struct sockaddr_in);
	uint32_t ip;
    
	ExtraInfo * extrainfo = (ExtraInfo*) calloc(1, sizeof(ExtraInfo));
	if (extrainfo == NULL) {
		return NULL;
	}
	Direccion.sin_family = AF_INET;
	bzero((void *) (((struct sockaddr *) &Direccion)->sa_data), 14);
	extrainfo->okNick = extrainfo->okPass = extrainfo->chatting = 0;
	getsockname(desc, (struct sockaddr*) &Direccion, &len);
	host = gethostbyaddr(&Direccion, len , AF_INET);
	if (host != (struct hostent *)0) {
		strcpy(extrainfo->host, host->h_name);
	} else {
		free(extrainfo);
		return NULL;
	}
	extrainfo->desc = desc;
	extrainfo->port = ntohs((uint16_t) Direccion.sin_port);

	ip = ntohl((uint32_t) Direccion.sin_addr.s_addr);
	sprintf(extrainfo->ip,"%d.%d.%d.%d",(&ip)[0], (&ip)[1], (&ip)[2], (&ip)[3]);
	return extrainfo;
}

void freeInfo(ExtraInfo * extrainfo) {
    if (extrainfo)
		free(extrainfo);
}

void send_ping_by_fd (int desc) {
    char * ans;
    IRCMsg_Ping (&ans, NULL, SERVER_NAME, NULL);
    send_msg(desc, ans, strlen(ans));
    free(ans);
}

void freeUser(char* username) {
	IRCTAD_Quit (username);
}

