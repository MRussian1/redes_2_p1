#include <stdio.h>
#include "../includes/list.h"
#define ELEMS 3

int copy_int(void* dst, void* src) {
	if (dst == NULL || src == NULL) {
		return -1;
	}
	printf("Copiando de %p a %p\n", src, dst);
	memcpy(dst, src, sizeof(int));
	return 0;
}

int eq_int (void* t1, void* t2) {
	if (t1 == NULL || t2 == NULL) {
		return t1 != t2; //0 si son iguales
	}
	printf("Comparando %i y %i\n", *((int*)t1), *((int*)t2));
	return *((int*)t1) != *((int*)t2);
}

void destroy_int(void* t1) {
}

int main() {
	_LIST int_list;
	if (setCopyFcn(&copy_int) == ERR
			|| setEqualFcn(&eq_int) == ERR
			|| setDestroyFcn(&destroy_int) == ERR) {
		puts("funciones nulas");
		return EXIT_FAILURE;
	} else if (iniList(&int_list) == ERR) {
		perror("Error al reservar memoria para la lista");
		return EXIT_FAILURE;
	}
	int i;
	ELELIST* p;
	for (i = ELEMS; i > -1; i--) {
		p = iniEleList(&i, sizeof(int));
		if (insertFirstEleList(&int_list, p) == ERR) {
			printf("Error insertando %i\n", i);
		}
		destroyEleList(p);
		free(p);
	}
	ELELIST *n = iniEleList(&i, sizeof(int));
	if (n == NULL) {
		puts("No se obtuvo memoria para n");
	} else {
		for (i = ELEMS; i > -1; i--) {
			*((int*)(getElelistInfo(n))) = i;
			p = searchInList(&int_list, n);
			if (p == NULL) {
				printf("No encontrado %i\n", i);
			} else {
				printf("Guardado %i:%i\n", i, *((int*)(getElelistInfo(p))));
			}
		}
	}
	destroyEleList(n);
	free(n);
	p = iniEleList(&i, sizeof(int));
	for (i = numElemsList(&int_list)-1; i>-1; i--) {
		extractNthEleList(&int_list, p, i, FALSE);
		printf("Extraido %i:%i\n", i, *((int*)(getElelistInfo(p))));
		destroyEleList(p);
	}
	free(p);
	freeList(&int_list);
	return 0;
}
