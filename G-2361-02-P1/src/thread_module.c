#include "../includes/thread_module.h"

_LIST client_thread_list;
pthread_mutex_t thread_list_lock;
int ping_timeout;					//to be set by this module's user via master_setup

//TODO: fix broken modularity through intefacing
extern int server_init_fd;
extern int recv_msg(int connval, char * msg, int len);
extern MSG_EXIT parseMessage(char * question, int desc,ExtraInfo * extrainfo);
extern void send_ping_by_fd(int desc);

/* this variable will be assigned and cleared by a signal handler
 * in order to stop other handlers from preventing atomic execution if
 * it's not critical to*/
int was_deferred = 0;

typedef struct {
	pthread_t id;
	int master_cmd_terminate;	// master needs the thread to close
	int conn_terminate : 1;			// connection has been closed and will promptly terminate
	int has_terminated : 1;			// thread has ended, master can join with thread and free info
	int needs_pong : 1;				// master has sent a ping, but pong has yet to come
	int service_fd;					// file descriptor associated with current connection
	char* username;					// to be set up when the username is confirmed
	int last_active_time;			// time since last non-PONG message reception
    ExtraInfo* extinf;              // extra info to be handled and managed by the IRC module
} client_thread_info;


//Cabeceras internas
int copy_thread(void* dst, void* src);
STATUS destroy_thread_info(client_thread_info* info);
uint32_t HashFNV1(void* e);

void masterHandleAlarm(int nsignal);
void threadHandleMaster (int signal);
void* handle_client(void* voidp);

int eq_thread (void* t1, void* t2);
int eq_service_fd (void* t1, void* t2);
int eq_username (void* t1, void* t2);
client_thread_info* getThreadInfoWith(const client_thread_info* key,
		int (*searchFn)(void*, void*));
client_thread_info* getThreadInfo(pthread_t id);
client_thread_info* getThreadInfoByFD (int fd);
client_thread_info* getThreadInfoByUsername(char* usrn);


//API principal

STATUS setup_master(int ping) {
	if (ping < 1) {
		return ERR;
	}
	ping_timeout = ping;
	
	struct sigaction act;
	bzero(&act, sizeof(act));
	act.sa_handler = &masterHandleAlarm;
	act.sa_flags = SA_RESTART;
	
	//Asignacion de manejadors de señales
	if (sigaction(SIGALRM, &act, NULL) != 0) {
        syslog(LOG_EMERG, "Al establecer el manejador de SIGALRM: %s",
				strerror(errno));
        return ERR;
    }
    act.sa_handler = &clean_end;
    act.sa_flags = 0;
    if (sigaction(SIGINT, &act, NULL) != 0) {
		syslog(LOG_ERR, "Al establecer el manejador de SIGINT: %s",
				strerror(errno));
        return ERR;
	}
	//inicio del mutex para acceder a la lista
	
	if (pthread_mutex_init(&thread_list_lock, NULL) != 0) {
		syslog(LOG_ERR, "Al inicializar el mutex de la lista: %s",
				strerror(errno));
        return ERR;
	}
    
    //inicializacion de funciones para manejar hilos como ELELISTs
    if (setCopyFcn(&copy_thread) == ERR
			|| setEqualFcn(&eq_thread) == ERR
			|| setHashFcn(&HashFNV1) == ERR) {
    	syslog(LOG_EMERG, "Funciones de ELELIST a NULL");
    	return ERR;
    } else if (iniList(&client_thread_list) == ERR) {
		syslog(LOG_ERR, "Error al reservar memoria para la lista");
		return ERR;
	}
	alarm(ping_timeout);
	return OK;
}

STATUS spawn_thread (int fd) {
	
	if (fd < 0) {
		return ERR;
	}
	pthread_t thread;
	
	client_thread_info ci;
	bzero(&ci, sizeof(client_thread_info));
	ci.service_fd = fd;
	ci.last_active_time = time(NULL);
	
	pthread_mutex_lock(&thread_list_lock);
	if (pthread_create(&thread, NULL, handle_client, "void") != 0) {
		syslog(LOG_ALERT, "Error al crear un hilo para atender al cliente\n");
		return ERR;
	}
	ci.id = thread;
	//guarda la lista de hilos activos
	ELELIST* e = iniEleList(&ci, sizeof(client_thread_info));
	if (e == NULL) {
		syslog(LOG_ALERT, "Error al reservar memoria para datos del hilo\n");
		pthread_mutex_unlock(&thread_list_lock);
		return ERR;
	}
	if (insertFirstEleList(&client_thread_list, e) == ERR) {
		syslog(LOG_ALERT, "Error al insertar hilo en la lista\n");
		destroyEleList(e);
		free(e);
		pthread_mutex_unlock(&thread_list_lock);
		return ERR;
	}
	pthread_mutex_unlock(&thread_list_lock);
	destroyEleList(e);
	free(e);
	return OK;
}

STATUS tellUsername(int fd, char* usrn) {
	if (fd < 0 || usrn == NULL) {
		return ERR;
	}
	
	client_thread_info* cip = getThreadInfoByFD(fd);
	if (cip == NULL) {
		return ERR;
	} else {
		cip->username = strdup(usrn);
		if (cip->username == NULL) {
			return ERR;
		}
	}
	return OK;
}

int last_active(int fd) {
	if (fd < 0) {
		return -1;
	}
	
	client_thread_info* cip = getThreadInfoByFD(fd);
	if (cip == NULL) {
		return -1;
	}
	return cip->last_active_time;
}

int getFDByUsername(char* usrn) {
	client_thread_info* cip = getThreadInfoByUsername(usrn);
	if (cip == NULL) {
		return -1;
	}
	return cip->service_fd;
}

//**************Funciones para trabajar con TAD ELELIST***************//
int copy_thread(void* dst, void* src) {
	if (dst == NULL || src == NULL) {
		return -1;
	}
	
	memcpy(dst, src, sizeof(client_thread_info));
	return 0;
}

int eq_thread (void* t1, void* t2) {
	if (t1 == NULL || t2 == NULL) {
		return t1 != t2; //0 si son iguales
	}
	return ((client_thread_info*)t1)->id != 
			((client_thread_info*)t2)->id;
}

int eq_service_fd (void* t1, void* t2) {
	if (t1 == NULL || t2 == NULL) {
		return t1 != t2; //0 si son iguales
	}
	return ((client_thread_info*)t1)->service_fd != 
			((client_thread_info*)t2)->service_fd;
}

int eq_username (void* t1, void* t2) {
	if (t1 == NULL || t2 == NULL) {
		return t1 != t2; //0 si son iguales
	}
	char* u1 = ((client_thread_info*)t1)->username;
	char* u2 = ((client_thread_info*)t2)->username;
	if (u1 == NULL || u2 == NULL) {
		return u1 != u2;
	}
	return strcmp(u1, u2);
}

//http://cbloomrants.blogspot.com.es/2010/11/11-29-10-useless-hash-test.html
uint32_t HashFNV1(void* e) {
	if (e == NULL) {
		return -1;
	}
    uint32_t hash = 2166136261;
    char key_string[10], *key = key_string;
    sprintf(key, "%d", *((int*)e));
    while(*key)
        hash = (16777619 * hash) ^ (*key++);
    return hash;
}

//*********************Manejadores de señales************************//
void masterHandleAlarm(int nsignal) {
	alarm(ping_timeout);
	was_deferred = nsignal;
	
	int i, num_threads = numElemsList(&client_thread_list);
	ELELIST* e = iniEleList(NULL, 0);
	time_t tm;
	if (e == NULL) {
		syslog(LOG_ERR, "No se inicializo elemento para buscar hilos\n");
	}
	client_thread_info *info;
	
	for (i = num_threads - 1; i > -1; i--) {
		extractNthEleList(&client_thread_list, e, i, TRUE);
		
		info = (client_thread_info*) getElelistInfo(e);	//A copy of the real structure
		if (info == NULL) {
			syslog(LOG_ERR, "Informacion de hilo vacia\n");
			destroyEleList(e);
			free(e);
			return;
		}
		if (info->has_terminated) {	//join ended threads
			pthread_join(info->id, NULL);
			extractNthEleList(&client_thread_list, e, i, FALSE);
			destroyEleList(e);
			continue;
		} else if (info->master_cmd_terminate) {
			pthread_cancel(info->id);	//thread has hung and should close
		} else if (info->needs_pong) {
			info->master_cmd_terminate = 1;
			pthread_kill(info->id, SIGUSR1);
		} else {
			tm = time(NULL);
            if ((tm - info->last_active_time) > ping_timeout) {
				info->needs_pong = 1;
			    //send ping
                send_ping_by_fd(info->service_fd);
            }
		}
	}
	if (getElelistLength(e) == 0) {	//No thread has been searched, free buffer
		destroyEleList(e);
	}
	free(e);
	if (was_deferred != nsignal) { //some other signal was deferred by this handler
		was_deferred = 0;
		raise(was_deferred);
	}
	was_deferred = 0;
}

//kept to prevent dying when interrupted and force stop socket waits
void threadHandleMaster (int signal) {
	destroy_thread_info(getThreadInfo(pthread_self()));
}

void clean_end (int nsignal) {
	alarm(0);
	if (was_deferred) {
		was_deferred = nsignal;
		return;
	} else {
		was_deferred = nsignal;
	}
	ELELIST* e = iniEleList(NULL, 0);
	client_thread_info* info;
	struct timespec ts;
	ts.tv_nsec = 0;
	int ret = 0;
	
	while (!isEmptyList(&client_thread_list)) {
		extractFirstEleList(&client_thread_list, e);
		info = (client_thread_info*) getElelistInfo(e);
		if (info == NULL) {
			syslog(LOG_ERR, "Informacion de hilo vacia");
			destroyEleList(e);
			free(e);
			continue;
		}
		info->master_cmd_terminate = 1;
		pthread_kill(info->id, SIGUSR1);
		
		ts.tv_sec = time(NULL) + THREAD_JOIN_TIMEOUT;
		ts.tv_nsec = 0;
		ret = pthread_timedjoin_np(info->id, NULL, &ts);
		if (ret != 0) {
			pthread_cancel(info->id);
			pthread_join(info->id, NULL);
		}
	}
	destroyEleList(e);
	free(e);
	freeList(&client_thread_list);
	pthread_mutex_destroy(&thread_list_lock);
	close(server_init_fd);
	exit(EXIT_SUCCESS);
}

//**********************Función de cada hilo*************************//
STATUS destroy_thread_info(client_thread_info* info) {
	if (info == NULL) {
		return ERR; 
	}
	
	if (info->username) {
		freeUser(info->username);
		free (info->username);
		info->username = NULL;
	}
	if (info->extinf != NULL) {
		freeInfo(info->extinf);
		info->extinf = NULL;
	}
    if (info->service_fd > -1) {
		close(info->service_fd);
		info->service_fd = -1;
	}
	
	return OK;
}

void* handle_client(void* voidp) {

	struct sigaction act;
	bzero(&act, sizeof(act));
	act.sa_handler = &threadHandleMaster;
	
	if (sigaction(SIGUSR1, &act, NULL) != 0) {
		syslog(LOG_ALERT, "Error al cambiar el manejador de la señal SIGUSR1\n");
		return NULL;
	}
	
	pthread_mutex_lock(&thread_list_lock);
	client_thread_info* info = getThreadInfo(pthread_self());
	pthread_mutex_unlock(&thread_list_lock);
	if (info == NULL) {
		syslog(LOG_ERR, "No se encontró información del hilo\n");
		return NULL;
	}
    info->extinf = extraInfoCreate(info->service_fd);
    if (info->extinf == NULL) {
        syslog(LOG_ERR, "No se pudo obtener informacion del cliente\n");
		info->has_terminated = 1;
		return NULL;
    }

	time_t tm;
    ssize_t mlen;
	char buff[MAX_MSG_LEN];
    MSG_EXIT msg_type;
    
	while (!(info->master_cmd_terminate)
			&& !(info->conn_terminate)) {
		//recv_msg
		bzero(buff, MAX_MSG_LEN);
		mlen = recv_msg(info->service_fd, buff, MAX_MSG_LEN);
        if (mlen < 1) {
            info->conn_terminate = 1;
            continue;
        }
        tm = time(NULL);
		//parse & send
        msg_type = parseMessage(buff, info->service_fd, info->extinf);
		
		switch(msg_type) {
			case __CLOSE:
				info->conn_terminate = 1;
				break;
            default:
                info->needs_pong = 0;
                info->last_active_time = tm;
		}
	}
	destroy_thread_info(info);
	info->has_terminated = 1;
	return NULL;
}

//****************Funciones de busqueda por atributos****************//

client_thread_info* getThreadInfoWith (const client_thread_info* key,
		int(*searchFn)(void*, void*)) {
	//busca su información global
	ELELIST* tmp_e = iniEleList((void*)key, sizeof(client_thread_info));
	if (tmp_e == NULL) {
		syslog(LOG_ALERT, "Error al obtener memoria para buscar el hilo\
		 en la lista\n");
		return NULL;
	} else if (setEqualFcn(searchFn) == ERR) {
		destroyEleList(tmp_e);
		free(tmp_e);
		return NULL;
	}
	const ELELIST* self_e = searchInList(&client_thread_list, tmp_e);
	destroyEleList(tmp_e);
	free(tmp_e);
	if (self_e == NULL) {
		syslog(LOG_ALERT, "No se ha encontrado el hilo en la lista\n");	
		return NULL;
	}
	
	if (setEqualFcn (&eq_thread) == ERR) {
		syslog(LOG_EMERG, "Cambio irreversible de funcion de comparacion");
		return ERR;
	} else if (self_e == NULL) {
		return ERR;
	}
	
	return getElelistInfo(self_e);
}

client_thread_info* getThreadInfo (pthread_t id) {
	client_thread_info self;
	self.id = id;
	
	return getThreadInfoWith(&self, &eq_thread);
}

client_thread_info* getThreadInfoByFD (int fd) {
	client_thread_info self;
	self.service_fd = fd;
	return getThreadInfoWith(&self, &eq_service_fd);
}

client_thread_info* getThreadInfoByUsername(char* usrn) {
	client_thread_info self, *cip;

	if (usrn == NULL) {
		return NULL;
	}
	
	self.username = strdup(usrn);
	if (self.username == NULL) {
		return NULL;
	}
	cip = getThreadInfoWith(&self, &eq_username);
	free(self.username);
	return cip;
}
