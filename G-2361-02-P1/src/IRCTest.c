/*
 * File:   IRCTest.c
 * Author: ana
 *
 * Created on Feb 7, 2016, 10:19:38 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../includes/COserver.h"
#include "../includes/IRC.h"


/*
 * Simple C Test Suite
 */

void test1() {
    
    char array[500];
	char * ans=NULL;
 	int sock, desc, len;
  ExtraInfo *extra;
    printf("IRCTest test 1\n");
   // go_demon();
    sock=initiate_serverCO(AF_INET, 7, 20);
	desc=accept_connectionCO(sock);
     extra=extraInfoCreate(desc);
      printf("Hostname %s\n", extra->host);
	while((len=recv_msg(desc, array, 400))){
		parseMessage(array, &ans,&len,desc, extra);
		
       
	}
    freeInfo(extra);
  
    return ;
    
}

void test2() {
	/*int len, sock, desc;
    char array[500];
    printf("serverTest test 2\n");
   	
    
    
   
    
     sock=initiate_serverCO(AF_INET, 7, 20);
   
     desc=accept_connectionCO(sock);
	
    while((len=recv_msg(desc, array, 400))){

		send_msg(desc, array, len);
	}
    close(desc);*/
}
void testdemon() {
   
   /* int len, sock, desc;
    char array[500];
	 printf("serverTest test demon\n");
	go_daemon ();
   
    sock=initiate_serverCO(AF_INET, 7, 20);
   
    desc=accept_connectionCO(sock);

    while((len=recv_msg(desc, array, 400))){

		send_msg(desc, array, len);
	}
    close(desc);*/
}

int main(int argc, char** argv) {
	int a=atoi(argv[1]);
    printf("%%SUITE_STARTING%% serverTest\n");
    printf("%%SUITE_STARTED%%\n");
	
	switch(a){
	case 1:
		printf("%%TEST_STARTED%% test1 (serverTest)\n");
		test1();
		printf("%%TEST_FINISHED%% time=0 test1 (serverTest) \n");
		break;
	case 2:

		printf("%%TEST_STARTED%% test2 (serverTest)\n");
		test2();
		
		break;
	case 3:

		
		testdemon();
		
		break;
	}
    
    return (EXIT_SUCCESS);
}



