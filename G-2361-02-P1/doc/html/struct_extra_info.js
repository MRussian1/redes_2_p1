var struct_extra_info =
[
    [ "chatting", "struct_extra_info.html#a2d0a531696851ae07ddc335394e663d9", null ],
    [ "desc", "struct_extra_info.html#a07d6d643b6d6aff7618aafea8de81bb0", null ],
    [ "host", "struct_extra_info.html#a5c81a93f94705f4c8cbf421f313b9aa1", null ],
    [ "idle", "struct_extra_info.html#a42aac8aa590f22b9dd3751f00538e5cc", null ],
    [ "ip", "struct_extra_info.html#aef7c62bc41e98248f33d0bf949e995b3", null ],
    [ "nick", "struct_extra_info.html#aa84da29acd685b0b30de76dc104d9a08", null ],
    [ "okNick", "struct_extra_info.html#afd8a9ed60a5cf0b95992a0104157a70b", null ],
    [ "okPass", "struct_extra_info.html#af140866fb4d09ed7d2a0bb39017742cd", null ],
    [ "password", "struct_extra_info.html#a66c90fc3b29c683c7027bef72c573eaa", null ],
    [ "port", "struct_extra_info.html#a8e0798404bf2cf5dabb84c5ba9a4f236", null ]
];