var structclient__thread__info =
[
    [ "alarm_terminate", "structclient__thread__info.html#a99e7afbe1c1e17ecd8f4e0da56ff0d17", null ],
    [ "clk_edge", "structclient__thread__info.html#a31cd3c3fcd76c0aa2345deab3cdae0ca", null ],
    [ "conn_terminate", "structclient__thread__info.html#a453e2532b92f3f96976c18238ed6a6b2", null ],
    [ "extinf", "structclient__thread__info.html#aaaefba9ecb58ee76090731dd0383d38c", null ],
    [ "has_lock", "structclient__thread__info.html#a519f41156055a0ee1a665c2176b6db61", null ],
    [ "has_terminated", "structclient__thread__info.html#a8eb16e466aa5228fb8d9a86f1c4aa4a6", null ],
    [ "id", "structclient__thread__info.html#a2465fd4497a7402ad72da7d6e03f9934", null ],
    [ "info_lock", "structclient__thread__info.html#ad054c6e4ca2e880dc0ce39cbb17ad2be", null ],
    [ "last_active_time", "structclient__thread__info.html#a8a6e6571cb27167d8ba40d12cf0b16a0", null ],
    [ "master_cmd_terminate", "structclient__thread__info.html#a9543af7ae70da7b7a632520fb57e43af", null ],
    [ "needs_pong", "structclient__thread__info.html#a36f53f2032ce69c9a3856655f119e9e0", null ],
    [ "service_fd", "structclient__thread__info.html#aefd2c0ded763ba805c930cc55f2a52e8", null ],
    [ "username", "structclient__thread__info.html#a9b20c006bd90a09e1465fb668700e81d", null ]
];