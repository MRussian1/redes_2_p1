var types_8h =
[
    [ "BOOL", "types_8h.html#a3e5b8192e7d9ffaf3542f1210aec18dd", [
      [ "FALSE", "types_8h.html#a3e5b8192e7d9ffaf3542f1210aec18ddaa1e095cc966dbecf6a0d8aad75348d1a", null ],
      [ "TRUE", "types_8h.html#a3e5b8192e7d9ffaf3542f1210aec18ddaa82764c3079aea4e60c80e45befbb839", null ]
    ] ],
    [ "STATUS", "types_8h.html#a32c27cc471df37f4fc818d65de0a56c4", [
      [ "ERR", "types_8h.html#a32c27cc471df37f4fc818d65de0a56c4a0f886785b600b91048fcdc434c6b4a8e", null ],
      [ "OK", "types_8h.html#a32c27cc471df37f4fc818d65de0a56c4a2bc49ec37d6a5715dd23e85f1ff5bb59", null ]
    ] ]
];