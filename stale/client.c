#include "librerias.h"

uint16_t client_port = 8888;

int inicializar_cliente() {
        int sockval;
        long aux;
        struct sockaddr_in Direccion;
        
        puts("Creating socket");
        if( (sockval=socket(AF_INET, SOCK_STREAM, 0)) < 0) {
             syslog(LOG_ERR, "Error creating socket");
             exit(EXIT_FAILURE);   
        }
        
        Direccion.sin_family = AF_INET;
        Direccion.sin_port = htons(client_port);
        Direccion.sin_addr.s_addr = htonl(LOCALHOST);
        bzero((void *)&(Direccion.sin_zero), 8);
        
        puts ("Connecting to socket");
        if (connect(sockval, (struct sockaddr*)&Direccion, sizeof(Direccion)) < 0) {
                perror("Error connecting socket");
                exit(EXIT_FAILURE); 
        }
        
        aux=client_port+35;
        printf("Enviando %li\n", aux);
		send(sockval, &aux, sizeof(long), 0);
        recv(sockval, &aux, sizeof(long), 0);
		printf("Recibido %li\n", aux);
        
        return sockval;
}

int main(int argc, char** argv) {
		if (argc >= 2) {
			client_port = atoi(argv[1]);
		}
        int resultado = inicializar_cliente();
        fprintf(stdout, "el resultado es %d\n", resultado);
        return EXIT_SUCCESS;
}