#include "librerias.h"


int inicializar_servidor() {
        int sockval;
        struct sockaddr_in Direccion;
        
        syslog(LOG_INFO, "Creating socket");
        if( (sockval=socket(AF_INET, SOCK_STREAM, 0)) < 0) {
             syslog(LOG_ERR, "Error creating socket");
             exit(EXIT_FAILURE);   
        }
        
        Direccion.sin_family = AF_INET;
        Direccion.sin_port = htons(NFC_SERVER_PORT);
        Direccion.sin_addr.s_addr = htonl(INADDR_ANY);
        bzero((void *)&(Direccion.sin_zero), 8);
        
        syslog (LOG_INFO, "Binding socket");
        if (bind(sockval, (struct sockaddr*)&Direccion, sizeof(Direccion)) < 0) {
                syslog(LOG_ERR, "Error binding socket");
                exit(EXIT_FAILURE); 
        }
        
        syslog (LOG_INFO, "Listening conections");
        if (listen(sockval, MAX_CONECTIONS) < 0) {
                syslog(LOG_ERR, "Error listening");
                exit(EXIT_FAILURE); 
        }
        
        return sockval;
}

int aceptar_conexion(int sockval) {
        int desc, len;
        struct sockaddr Conexion;
        
        len = sizeof(Conexion);
        
        if ((desc = accept(sockval, &Conexion, &len)) < 0) {
             syslog(LOG_ERR, "Error accepting conection");
             exit(EXIT_FAILURE);
        }
        
       
        //wait_finished_services();
        
        return desc;
}

/*wait_finished_services(){
        wait();
                
        
}*/

void launch_service(int connval) {
        int pid;
        long type, aux;
        char lalal [500];
        //pid = fork();
        //if (pid < 0) exit(EXIT_FAILURE);
        //if (pid == 0) {
			syslog(LOG_INFO, "New access");
			//recibe mensaje de cliente
			recv(connval, lalal, sizeof(char)*400, 0);
			//echo
			
			send(connval, lalal, strlen(lalal), 0);
			type = ntohl(aux);
			//procesar mensaje
			//enviar mensaje a destinatarios
			//comprobar si cliente emisor esta vivo
			//cerrar conexion si no
			//database_access(connval, type, NULL);
			//close(connval);
			//syslog(LOG_INFO, "Exiting service");
			//exit(EXIT_SUCCESS);
			return;
		//}
		//debe conocr a sus hijos y comprobar los que ya han terminado para liberar sus recursos
       // wait(0);
        
}

int main() {
		int desc;
        int resultado = inicializar_servidor();
        desc=aceptar_conexion(resultado);
		while(1){
		 launch_service(desc);
		}
        fprintf(stdout, "el resultado es %d\n", resultado);
        return EXIT_SUCCESS;
}
