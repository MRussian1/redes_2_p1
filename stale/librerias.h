#ifndef _LIBRERIAS_H
#define _LIBRERIAS_H


#include <sys/socket.h>
#include <string.h>
#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>



#define NFC_SERVER_PORT 6667
#define LOCALHOST 0xC0A80102


#define MAX_CONECTIONS 10

int inicializar_servidor();
int main();
int aceptar_conexion(int sockval);
void launch_service(int connval);

#endif
