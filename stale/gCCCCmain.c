#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
//#include <signal.h>


/*
 void handleSignal(int nsignal) {
	//cerrar socket
 }
 * */
//http://stackoverflow.com/questions/3095566/linux-daemonize
//se puede usar daemon() (man 3 daemon) y añadir la funcionalidad faltante?
int daemonizar (char* serivce_id) {
	pid_t pid = fork();
	if (pid < 0) {
		//error
	} else if (pid == 0) {
		//crear una nueva sesión de tal forma que el proceso pase a ser el líder de sesion
		if (setsid() < 0) {
			perror("Al crear sesion");
			return -1;
		}
		//cambiar la máscara de modo de ficheros para que sean accesibles a cualquiera
		umask(0);
		//establecer el directorio raíz / como directorio de trabajo
		if (chdir("/") < 0) {
			perror("Al cambiar de directorio");
			return -1;
		}
		//cerrar todos los descriptores de fichero que pueda haber abiertos incluidos stdin, stdout, stder
			//buscar posibles otros y cerrar en bucle?
		if (close(stdin) < 0 || fclose(stdout) < 0 || fclose(stderr) < 0) {
			perror("Al cerrar descriptores");
			return -1;
		}
		//abrir el log del sistema para su uso posterior
		openlog(serivce_id, LOG_PID /*option*/, LOG_USER);
		
		//alarm(30); //cierra conexion despues de 30 segundos sin respuesta
		//alarm(0);	//cancel alarma si (condicion?)
		return 0;
	}
	return 0;
}

int main(int argc, char** argv) {
	/*if(signal(SIGALRM, handleSignal) == SIG_ERR) {
        perror("Al establecer el manejador de SIGALRM");
        return EXIT_FAILURE;
    }*/
	return 0;
}